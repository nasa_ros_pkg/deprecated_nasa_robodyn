/**
 * @file RosMsgTrajectoryFactory.h
 * @brief Defines the RosMsgTrajectoryFactory classes.
 * @author Ross Taylor
 * @date Nov 1, 2012
 */

#ifndef ROS_MSG_TRAJECTORY_FACTORY_H
#define ROS_MSG_TRAJECTORY_FACTORY_H

#include <nasa_robodyn_controllers_core/TrajectoryFactory.h>
#include <nasa_robodyn_controllers_core/RosMsgTrajectory.h>

#include <boost/concept/assert.hpp>
#include <boost/concept_check.hpp>

#include "nasa_r2_common_msgs/ControllerJointSettings.h"
#include "nasa_r2_common_msgs/ControllerPoseSettings.h"
#include <sensor_msgs/JointState.h>
#include "nasa_r2_common_msgs/PoseState.h"

#include <stdexcept>
#include "nasa_common_utilities/Logger.h"
#include "nasa_robodyn_utilities/RosMsgConverter.h"

class RosMsgJointTrajectoryFactory
{
public:
    RosMsgJointTrajectoryFactory() {}
    virtual ~RosMsgJointTrajectoryFactory() {}

    void setFactory(boost::shared_ptr<JointTrajectoryFactory<> > factory_in)
    {
        factory = factory_in;
        sequenceFactory.setTrajectoryFactory(factory);
        motionLimiter.setJointMotionLimiter(factory);
    }

    void setSettings(const nasa_r2_common_msgs::ControllerJointSettings& settings);
    void setLimits(const std::vector<std::string>& jointNames, double velLim, double accLim);
    void setPositionLimiter(boost::shared_ptr<JointNamePositionLimiter> posLimiter);

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param jointPositions start joint positions
     * @param jointVels start joint velocities
     * @param prevJointVels previous joint velocities (joint states only hold pos & vel so the prev is used to derive acc)
     * @param goalTraj goal joint positions
     */
    virtual boost::shared_ptr<RosMsgJointTrajectory> getTrajectory(const sensor_msgs::JointState& jointPositions,
                                                                   const sensor_msgs::JointState& jointVels,
                                                                   const sensor_msgs::JointState& prevJointVels,
                                                             const trajectory_msgs::JointTrajectory& goalTraj);

protected:
    typedef JointTrajectorySequenceFactory<> sequenceFactoryType;
    sequenceFactoryType sequenceFactory;
    boost::shared_ptr<JointTrajectoryFactory<> > factory;
    JointNameMotionLimiter motionLimiter;
    boost::shared_ptr<JointNamePositionLimiter> positionLimiter;
};

class RosMsgCartesianTrajectoryFactory
{
public:
    RosMsgCartesianTrajectoryFactory() {}
    virtual ~RosMsgCartesianTrajectoryFactory() {}

    void setFactory(boost::shared_ptr<CartesianTrajectoryFactory<SynchedTrajectory> > factory_in)
    {
        factory = factory_in;
        sequenceFactory.setTrajectoryFactory(factory);
    }

    void setSettings(const nasa_r2_common_msgs::ControllerPoseSettings& settings);

    /**
     * @brief getTrajectory
     * @param poseState current pose
     * @param goalTraj goal trajectory
     */
    virtual boost::shared_ptr<RosMsgSynchedCartesianTrajectory> getTrajectory(const nasa_r2_common_msgs::PoseState& poseState,
                               const nasa_r2_common_msgs::PoseTrajectory& goalTraj);

protected:
    typedef CartesianTrajectorySequenceFactory<SynchedTrajectory> sequenceFactoryType;
    sequenceFactoryType sequenceFactory;
    boost::shared_ptr<CartesianTrajectoryFactory<SynchedTrajectory> > factory;
};

#endif
