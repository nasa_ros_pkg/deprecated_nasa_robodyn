/**
 * @file RosMsgTreeFk.h
 * @brief Defines the RosMsgTreeFk class.
 * @author Ross Taylor
 */
#ifndef TREE_FK_H
#define TREE_FK_H

#include "nasa_robodyn_controllers_core/KdlTreeFk.h"
#include "nasa_r2_common_msgs/PoseState.h"
#include <sensor_msgs/JointState.h>
#include "nasa_robodyn_utilities/RosMsgConverter.h"
#include <tf_conversions/tf_kdl.h>

class RosMsgTreeFk : public KdlTreeFk
{
public:
    RosMsgTreeFk();
    ~RosMsgTreeFk();

    void reset()
    {
        prevVel.clear();
    }

    bool getPoseState(const sensor_msgs::JointState& jointState, nasa_r2_common_msgs::PoseState& poseState);

protected:
    virtual void initialize();

private:
    typedef std::map<std::string, std::pair<ros::Time, geometry_msgs::Twist> > prevVel_type;
    prevVel_type prevVel;
    std::vector<std::string> jointNames;
    KDL::JntArrayVel joints;
    std::map<std::string, KDL::FrameVel> poseMap;
};

#endif
