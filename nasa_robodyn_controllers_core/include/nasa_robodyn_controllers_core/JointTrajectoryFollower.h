/**
 * @file JointTrajectoryFollower.h
 * @brief Defines the JointTrajectoryFollower classes.
 * @author Ross Taylor
 * @date Aug 28, 2012
 */

#ifndef JOINT_TRAJECTORY_FOLLOWER_H
#define JOINT_TRAJECTORY_FOLLOWER_H

#include <vector>
#include <trajectory_msgs/JointTrajectory.h>
#include <sensor_msgs/JointState.h>
#include "nasa_robodyn_controllers_core/GoalManager.h"
#include "nasa_robodyn_controllers_core/RosMsgTrajectory.h"

/**
 * @class JointTrajectoryFollower
 * @brief Provides a an abstract class for breadcrumbing a trajectory and maintaining status
 */
class JointTrajectoryFollower
{
public:
    JointTrajectoryFollower();
    virtual ~JointTrajectoryFollower() {}

    virtual const std::vector<std::string>& getJointNames() const = 0;

    virtual double getDuration() const = 0;

    /**
     * @brief isActive
     * @return bool active state
     * @details false if done with trajectory or reset, true otherwise
     */
    inline virtual bool isActive() const {return goalManager.isReady();}

    /**
     * @brief abort stops trajectory and sets status to aborted
     */
    virtual void abort(const std::string& msg = std::string());
    /**
     * @brief preempt stops trajectory and sets status to preempted
     */
    virtual void preempt(const std::string& msg = std::string());

    /**
     * @brief get next point
     * @param nextPoint joint state of next point, returned
     * @param timeFromStart time from start of next point, returned
     * @return void
     */
    virtual void getNextPoint(sensor_msgs::JointState& nextPoint, ros::Duration& timeFromStart) = 0;

    GoalManager goalManager;
    double velocityFactor;
};

/**
 * @class PreplannedJointTrajectoryFollower
 * @brief Provides a class for breadcrumbing a preplanned trajectory
 */
class PreplannedJointTrajectoryFollower : public JointTrajectoryFollower
{
public:
    PreplannedJointTrajectoryFollower();
    virtual ~PreplannedJointTrajectoryFollower() {}

    inline virtual const std::vector<std::string>& getJointNames() const {return trajectory.joint_names;}

    inline virtual double getDuration() const {return trajectory.points.back().time_from_start.toSec();}

    /**
     * @brief set trajectory
     * @param trajectory_in new trajectory
     * @return void
     * @details if there is an existing trajectory, it is preempted
     */
    void setTrajectory(const trajectory_msgs::JointTrajectory& trajectory_in, double velocityFactor_in = 1.);

    /**
     * @brief get next point
     * @param nextPoint joint state of next point, returned
     * @param timeFromStart time from start of next point, returned
     * @return void
     */
    virtual void getNextPoint(sensor_msgs::JointState& nextPoint, ros::Duration& timeFromStart);

private:
    trajectory_msgs::JointTrajectory trajectory;
    trajectory_msgs::JointTrajectory::_points_type::const_iterator trajIt;
};

/**
 * @class OnlineJointTrajectoryFollower
 * @brief Provides a class for breadcrumbing an online trajectory
 */
class OnlineJointTrajectoryFollower : public JointTrajectoryFollower
{
public:
    OnlineJointTrajectoryFollower();
    virtual ~OnlineJointTrajectoryFollower() {}

    inline virtual const std::vector<std::string>& getJointNames() const {return trajectory->getJointNames();}

    inline virtual double getDuration() const {return trajectory->getDuration();}

    /**
     * @brief set trajectory
     * @param trajectory_in new trajectory
     * @return void
     * @details if there is an existing trajectory, it is preempted
     */
    void setTrajectory(boost::shared_ptr<RosMsgJointTrajectory> trajectory_in, double timestep_in, double velocityFactor_in = 1.);

    /**
     * @brief get next point
     * @param nextPoint joint state of next point, returned
     * @param timeFromStart time from start of next point, returned
     * @return void
     */
    virtual void getNextPoint(sensor_msgs::JointState& nextPoint, ros::Duration& timeFromStart);

private:
    boost::shared_ptr<RosMsgJointTrajectory> trajectory;
    double time, timestep;
};

#endif
