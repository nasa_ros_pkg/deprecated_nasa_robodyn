#ifndef CARTESIAN_HYBCNTRL_H
#define CARTESIAN_HYBCNTRL_H

#include <kdl/treefksolverpos_recursive.hpp>
#include <kdl/treefksolver.hpp>
#include <kdl/jntarrayvel.hpp>
#include <kdl/framevel.hpp>
#include <kdl/frames.hpp>

#include <iostream>
#include <Eigen/Core>

#include "nasa_robodyn_controllers_core/JointTrajectoryFollower.h"
#include "nasa_robodyn_controllers_core/KdlTreeUtilities.h"
#include "nasa_robodyn_utilities/RateLimiter.h"
#include "nasa_r2_common_msgs/ForceControlAxis.h"
#include "nasa_robodyn_utilities/GeneralUtilities.h"

using namespace std;
using namespace Eigen;

class Cartesian_HybCntrl
{
public:

    //! @brief defines the current force control in the force control frame
    struct ForceControl
    {
        std::string sensorName;
        bool x;
        bool y;
        bool z;
        bool roll;
        bool pitch;
        bool yaw;
        KDL::Wrench desiredForce;
        KDL::Wrench sensorForce;
        KDL::Wrench integrator;
        KDL::Wrench derivative;
        KDL::Wrench gain;
        KDL::Wrench error;
        KDL::Twist prevVelocity;
        KDL::Frame pose;
        ForceControl()
        {
            x = y = z = roll = pitch = yaw = false;
            desiredForce = KDL::Wrench::Zero();
            sensorForce = KDL::Wrench::Zero();
            integrator = KDL::Wrench::Zero();
            derivative = KDL::Wrench::Zero();
            gain = KDL::Wrench::Zero();
            error = KDL::Wrench::Zero();
            prevVelocity = KDL::Twist::Zero();
            pose = KDL::Frame::Identity();
        }
    };

    Cartesian_HybCntrl();
    ~Cartesian_HybCntrl();

    void setGain(double forceP, double torqueP, double forceI, double torqueI, double forceD, double torqueD, double forceWindup, double torqueWindup);
    void setPoseSettings(double linVel, double angVel, double linAcc, double angAcc);
    void setFilter(double filterAlpha);
    void setSensorNameMap(const std::map<std::string, std::string>& sensorNameMap);
    void updateSensorForces(const std::map<std::string, KDL::Wrench> &sensorForces);
    bool setInitialPose(const std::string& frameName, const::KDL::Frame& pose);
    void createForceControlMap(std::map<std::string, std::pair<std::vector<int>, std::vector<double> > > forceNodes);

    virtual void updateFrames(std::vector<std::string> nodeNames, std::map<std::string, KDL::Frame> referenceFrameMap, std::vector<KDL::Frame> &nodeFrames, double dt);

    bool doFeedForward;

protected:

    void limitVector(double minVal, double maxVal, KDL::Vector &vector)
    {
        int i = 0;
        for(; i<3; ++i)
        {
            Robodyn::limit(minVal, maxVal, vector[i]);
        }
    }

    void limitVelocity(const KDL::Twist &pastVelocity, KDL::Twist &currVelocity)
    {
        for(int i = 0; i< 3; ++i)
        {
            currVelocity.vel(i) = linearRateLimiter->getLimitedValue(currVelocity.vel(i), pastVelocity.vel(i));
            currVelocity.rot(i) = angularRateLimiter->getLimitedValue(currVelocity.rot(i), pastVelocity.rot(i));
            limitVector(-maxLinearVelocity, maxLinearVelocity, currVelocity.vel);
            limitVector(-maxAngularVelocity, maxAngularVelocity, currVelocity.rot);
        }
    }

    std::string getSensorNameFromNode(const std::string& nodeName)
    {
        for(std::map<std::string, std::string>::iterator itr = sensorNameMap.begin(); itr != sensorNameMap.end(); ++itr)
        {
            if(nodeName.find(itr->first)!= std::string::npos)
            {
                //! if sensor name in map, return sensor name
                return itr->second;
            }
        }

        //! otherwise return empty string
        RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::ERROR<<"Could not find sensor for "<<nodeName<<"!!";
        return "";
    }

    boost::shared_ptr<RateLimiter>  linearRateLimiter;
    boost::shared_ptr<RateLimiter>  angularRateLimiter;
    double                          fKp;
    double                          tKp;
    double                          fKi;
    double                          tKi;
    double                          fKd;
    double                          tKd;
    double                          maxLinearVelocity;
    double                          maxAngularVelocity;
    double                          forceIntegralWindupLimit;
    double                          torqueIntegralWindupLimit;
    double                          alpha;

    std::map<std::string, ForceControl> forceCntrlMap;
    std::map<std::string, std::string> sensorNameMap;
    std::map<std::string, ForceControl>::iterator forceItr;
    std::map<std::string, KDL::Wrench> sensorForceMap;
    std::map<std::string, KDL::Wrench>::const_iterator sensorForceItr;

private:
    std::string logCategory;
    KDL::Frame sensorFrame;
    KDL::Frame forceFrame;
    KDL::Wrench sensorForce;
    KDL::Frame forcePose;
    KDL::Frame desiredPose;
    KDL::Frame sensorPose;
    KDL::Wrench forceError;
    KDL::Wrench actualForce;
    KDL::Wrench forcePID;
    KDL::Twist velocity;
};

#endif // CARTESIAN_HYBCNTRL_H
