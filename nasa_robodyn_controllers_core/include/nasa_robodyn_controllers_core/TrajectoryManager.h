/**
 * @file TrajectoryManager.h
 * @brief Defines the TrajectoryManager class.
 * @author Ross Taylor
 */
#ifndef TRAJECTORYMANAGER_H
#define TRAJECTORYMANAGER_H

#include "nasa_robodyn_controllers_core/JointTrajectoryManager.h"
#include "nasa_robodyn_utilities/GoalStatusSender.h"
#include <sensor_msgs/JointState.h>
#include <boost/ptr_container/ptr_map.hpp>
#include "nasa_robodyn_controllers_core/JointTrajectoryFollower.h"
#include "nasa_r2_common_msgs/PoseTrajectory.h"
#include "nasa_r2_common_msgs/ControllerPoseSettings.h"
#include "nasa_r2_common_msgs/PoseState.h"
#include "nasa_r2_common_msgs/StringArray.h"
#include "nasa_robodyn_controllers_core/MobileTreeIk.h"
#include "nasa_robodyn_controllers_core/TrapezoidalVelocityTrajectoryFactory.h"
#include "nasa_robodyn_controllers_core/RosMsgTrajectoryFactory.h"
#include "nasa_robodyn_controllers_core/RosMsgTreeIkTrajectoryFactory.h"
#include "nasa_robodyn_controllers_core/RosMsgForceTrajectoryFactory.h"
#include "nasa_r2_common_msgs/JointCommand.h"
#include "nasa_r2_common_msgs/JointControlDataArray.h"
#include "nasa_r2_common_msgs/ReplanType.h"
#include "nasa_robodyn_controllers_core/MotionLimiter.h"
#include "nasa_r2_common_msgs/JointCapability.h"
#include "nasa_r2_common_msgs/ForceControlAxis.h"
#include "nasa_r2_common_msgs/WrenchState.h"
#include "nasa_r2_common_msgs/ForceControlAxisArray.h"

#include <iostream>
#include <string>

/**
 * @brief The TrajectoryManager class handles trajectories. A pure virtual writeJointState function
 * is used to actually send the next point.
 */
class TrajectoryManager : public JointTrajectoryManager, public GoalStatusSender
{
public:
    TrajectoryManager(const std::string &name);

    virtual ~TrajectoryManager();

    void initialize(const std::string &urdfFile, double timeStep_in);

    void updateTrajectories(const nasa_r2_common_msgs::JointCommand& defaultCommandMsg, const nasa_r2_common_msgs::JointControlDataArray& defaultControlMsg, const sensor_msgs::JointState& startJointPositions, const sensor_msgs::JointState& startJointVels);
    void updateSensorForces(const nasa_r2_common_msgs::WrenchState& wrenchSensors);
    void updateActualPoseState(const nasa_r2_common_msgs::PoseState& actualPoseState);
    void updateInertia(const sensor_msgs::JointState& inertia_in);

    void setJointSettings(const nasa_r2_common_msgs::ControllerJointSettings& settingsMsg);
    void setPoseSettings(const nasa_r2_common_msgs::ControllerPoseSettings& settingsMsg);
    void setJointCapabilities(const nasa_r2_common_msgs::JointCapability& capabilitiesMsg);
    void setBases(const nasa_r2_common_msgs::StringArray& basesMsg);
    void setVelocityFactor(double velocityFactor_in) {velocityFactor = velocityFactor_in;}
    void setPriorityTol(std::vector<double> priorityNum, std::vector<double> priorityLinearTol, std::vector<double> priorityAngularTol);
    void setIkParameters(double mBar, double kr, unsigned int maxIts, double maxTwist);
    void setSensorNameMap(std::vector<std::string> sensorKeys, std::vector<std::string> sensorNames);
    void setForceParameters(double filterAlpha, double forceGain, double torqueGain, double forceIntegral, double torqueIntegral, double forceDamping, double torqueDamping, double forceWindupLimit, double torqueWindupLimit,  bool doFeedForward);

    /**
     * @brief addCartesianWaypoints adds a Cartesian trajectory unless there are conflict, then it stops the conflicts and tells the caller
     *                              how long to wait before trying the trajectory again
     * @param trajectory trajectory to add
     * @param startJointPositions starting joint positions
     * @param startJointVels starting joint velocities
     * @param prevJointVels previous joint velocities
     * @param startPoseState starting Cartesian position
     * @param startPoseVels starting Cartesian velocity
     * @return time to wait for conflicts to be resolved
     */
    double addForceWaypoints(const nasa_r2_common_msgs::PoseTrajectory& trajectory,
                               const sensor_msgs::JointState& startJointPositions,
                               const sensor_msgs::JointState& startJointVels,
                               const sensor_msgs::JointState& prevJointVels,
                               const nasa_r2_common_msgs::PoseState& startPoseState,
                               const nasa_r2_common_msgs::PoseState& startPoseVels,
                               const nasa_r2_common_msgs::ForceControlAxisArray& forceAxes);
    double addCartesianWaypoints(const nasa_r2_common_msgs::PoseTrajectory& trajectory,
                               const sensor_msgs::JointState& startJointPositions,
                               const sensor_msgs::JointState& startJointVels,
                               const sensor_msgs::JointState& prevJointVels,
                               const nasa_r2_common_msgs::PoseState& startPoseState,
                               const nasa_r2_common_msgs::PoseState& startPoseVels);
    void addJointWaypoints(const trajectory_msgs::JointTrajectory& trajectory,
                           const sensor_msgs::JointState& startJointPositions,
                           const sensor_msgs::JointState& startJointVels,
                           const sensor_msgs::JointState& prevJointVels,
                           const std::vector<nasa_r2_common_msgs::ReplanType>& replanVec = std::vector<nasa_r2_common_msgs::ReplanType>());
    void addJointBreadcrumbs(const trajectory_msgs::JointTrajectory& trajectory);

protected:
    virtual void writeJointState(const sensor_msgs::JointState& jointState_out) = 0;
    virtual void writeJointControl(const nasa_r2_common_msgs::JointControlDataArray& jointControl_out) = 0;

    bool isActive(const actionlib_msgs::GoalID& goalId) const;

    // helper functions
    void abort(const actionlib_msgs::GoalID &goalId, const std::string &msg = std::string()); // abort if active
    void abort(const actionlib_msgs::GoalID& goalId, std::set<std::string>& abortedJoints, const std::string &msg = std::string()); // abort if active and return joint names aborted
    void preempt(const std::vector<std::string>& jointNames, std::set<std::string>& preemptedJoints, const std::string &msg = std::string()); // preempt if joint being used
    void resetAll(const std::string &msg = std::string());
    // kasquish is a smooth stop
    void kasquish(const std::vector<std::string>& jointNames,
                               const sensor_msgs::JointState& startJointPositions,
                               const sensor_msgs::JointState& startJointVels);
    void kasquishAll(const sensor_msgs::JointState& startJointPositions,
                     const sensor_msgs::JointState& startJointVels, const std::string &msg = std::string());

    std::string sensor;

    double velocityFactor;
    double timeStep;
    double stopVelocityLimit;
    double stopAccelerationLimit;

    bool jointSettingsValid, poseSettingsValid, basesValid;

private:
    struct GoalIdCompare : public std::binary_function<actionlib_msgs::GoalID, actionlib_msgs::GoalID, bool>
    {
        bool operator()(const actionlib_msgs::GoalID& left, const actionlib_msgs::GoalID& right) const
        {
            return (left.stamp < right.stamp || (left.stamp == right.stamp && left.id < right.id));
        }
    };

    // output
    sensor_msgs::JointState jointStateOut;
    nasa_r2_common_msgs::JointControlDataArray   jointControlOut;

    // trajectory helpers
    typedef TrapezoidalVelocityJointTrajectoryFactory jointTrajFactory_type;
    typedef TrapezoidalVelocitySynchedCartesianTrajectoryFactory cartTrajFactory_type;
    boost::shared_ptr<jointTrajFactory_type> jointTrajFactory;
    boost::shared_ptr<cartTrajFactory_type> cartTrajFactory;
    RosMsgJointTrajectoryFactory rosJointTrajFactory;
    RosMsgCartesianTrajectoryFactory rosCartTrajFactory;
    RosMsgTreeIkTrajectoryFactory rosTreeIkTrajFactory;
    RosMsgForceTrajectoryFactory rosForceTrajFactory;
    nasa_r2_common_msgs::ControllerJointSettings jointSettings;
    boost::shared_ptr<JointNamePositionLimiter> positionLimiter;

    // other helpers
    std::map<std::string, unsigned int> jointIndexMap;
    boost::shared_ptr<MobileTreeIk> treeIkPtr;
    boost::shared_ptr<Cartesian_HybCntrl> forceController;
    std::map<std::string, bool> controllableJoints;
    std::map<std::string, double> jointInertiaMap;
    std::map<std::string, KDL::Wrench> forceSensorMap;
    std::vector<std::string> actualFrameNames;
    std::map<std::string, KDL::FrameVel> actualFrameVelMap;

    // keep track of goals
    typedef boost::ptr_map<actionlib_msgs::GoalID, JointTrajectoryFollower, GoalIdCompare> GoalFollowerMap_type;
    GoalFollowerMap_type goalFollowerMap;
    typedef std::map<actionlib_msgs::GoalID, actionlib_msgs::GoalStatus, GoalIdCompare> GoalStatusMap_type;
    GoalStatusMap_type goalStatusMap;
    typedef std::map<actionlib_msgs::GoalID, std::vector<nasa_r2_common_msgs::ReplanType>, GoalIdCompare> GoalReplanMap_type;
    GoalReplanMap_type goalReplanMap;

    void setupJointState(const nasa_r2_common_msgs::JointCommand& jointCommand);
    void setupJointControl(const nasa_r2_common_msgs::JointControlDataArray& jointControl);
};

#endif
