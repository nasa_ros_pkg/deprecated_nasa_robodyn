/**
 * @file RosMsgTrajectory.h
 * @brief Defines the RosMsgTrajectory classes.
 * @author Ross Taylor
 * @date Nov 1, 2012
 */

#ifndef ROS_MSG_TRAJECTORY_H
#define ROS_MSG_TRAJECTORY_H

#include <nasa_robodyn_controllers_core/Trajectory.h>
#include <actionlib_msgs/GoalID.h>
#include <trajectory_msgs/JointTrajectoryPoint.h>
#include <nasa_r2_common_msgs/PoseTrajectoryPoint.h>
#include <nasa_r2_common_msgs/PriorityArray.h>

template <class P, class outType>
class RosMsgTrajectory : public Trajectory<outType>
{
public:
    RosMsgTrajectory() {}
    virtual ~RosMsgTrajectory() {}

    void setTrajectory(boost::shared_ptr<Trajectory<P> > traj_in)
    {
        setDuration(traj_in->getDuration());
        trajectory = traj_in;
    }

    inline const actionlib_msgs::GoalID& getGoalId() {return goalId;}

    void setGoalId(const actionlib_msgs::GoalID& goalId_in) {goalId = goalId_in;}
    void setGoalId(const ros::Time& stamp, const std::string& id)
    {
        goalId.stamp = stamp;
        goalId.id = id;
    }

protected:
    boost::shared_ptr<Trajectory<P> > trajectory;

private:
    actionlib_msgs::GoalID goalId;
};

/**
 * @class RosMsgJointTrajectory
 * @brief Wraps a JointTrajectory with ROS message handling
 */
class RosMsgJointTrajectory : public RosMsgTrajectory<JointTrajectory::poseType, trajectory_msgs::JointTrajectoryPoint>
{
public:
    RosMsgJointTrajectory() {}
    virtual ~RosMsgJointTrajectory() {}

    void setJointNames(const std::vector<std::string>& jointNames_in) {jointNames = jointNames_in;}

    inline const std::vector<std::string>& getJointNames() {return jointNames;}

    void getPose(double time, trajectory_msgs::JointTrajectoryPoint &pose);

private:
    std::vector<std::string> jointNames;
};

/**
 * @class RosMsgSynchedCartesianTrajectory
 * @brief Wraps a SynchedCartesianTrajectory with ROS message handling
 */
class RosMsgSynchedCartesianTrajectory : public RosMsgTrajectory<SynchedCartesianTrajectory::poseType, nasa_r2_common_msgs::PoseTrajectoryPoint>
{
public:
    RosMsgSynchedCartesianTrajectory() {}
    virtual ~RosMsgSynchedCartesianTrajectory() {}

    void setNodeNames(const std::vector<std::string>& nodeNames_in) {nodeNames = nodeNames_in;}
    void setRefFrames(const std::vector<std::string>& refFrames_in) {refFrames = refFrames_in;}
    void setPriorities(const std::vector<nasa_r2_common_msgs::PriorityArray>& priorities_in) {priorities = priorities_in;}

    inline const std::vector<std::string>& getNodeNames() {return nodeNames;}
    inline const std::vector<std::string>& getRefFrames() {return refFrames;}
    inline const std::vector<nasa_r2_common_msgs::PriorityArray>& getPriorities() {return priorities;}

    virtual void getPose(double time, nasa_r2_common_msgs::PoseTrajectoryPoint& stepPose);

private:
    std::vector<std::string> nodeNames;
    std::vector<std::string> refFrames;
    std::vector<nasa_r2_common_msgs::PriorityArray> priorities;
};

#endif
