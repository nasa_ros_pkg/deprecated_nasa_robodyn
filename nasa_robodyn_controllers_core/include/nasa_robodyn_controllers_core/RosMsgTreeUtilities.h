#ifndef ROSMSGTREEUTILITIES_H
#define ROSMSGTREEUTILITIES_H

#include <kdl/framevel.hpp>
#include <nasa_robodyn_controllers_core/KdlTreeUtilities.h>
#include <nasa_robodyn_utilities/RosMsgConverter.h>
#include <nasa_r2_common_msgs/StringArray.h>
#include <nasa_r2_common_msgs/PoseState.h>
#include "nasa_common_utilities/Logger.h"

using namespace RosMsgConverter;

class RosMsgTreeUtilities : public KdlTreeUtilities
{
public:
    RosMsgTreeUtilities();

    void loadFromFile(const std::string &fileName);

    bool setBaseFrame(const nasa_r2_common_msgs::StringArray &baseMsg);
    bool setBaseFrame(std::string &baseMsg);

    void getFrames(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::Frame> &frames);
    void getFrameVels(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::FrameVel> &framevels);
    void getFrameAccs(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::FrameAcc> &frameaccs);

    std::vector<std::string> bases;
    std::vector<std::string> jointNames;
};

#endif // ROSMSGTREEUTILITIES_H
