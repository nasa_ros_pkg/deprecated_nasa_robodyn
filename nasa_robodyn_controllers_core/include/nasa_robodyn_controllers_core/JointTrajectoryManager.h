/**
 * @file JointTrajectoryManager.h
 * @brief Defines the JointTrajectoryManager class.
 * @author Ross Taylor
 * @date Aug 29, 2012
 */

#ifndef JOINT_TRAJECTORY_MANAGER_H
#define JOINT_TRAJECTORY_MANAGER_H

#include <trajectory_msgs/JointTrajectory.h>
#include <actionlib_msgs/GoalID.h>
#include <map>

/**
 * @class JointTrajectoryManager
 * @brief Provides a helper for managing joint trajectories
 */
class JointTrajectoryManager
{
public:
    JointTrajectoryManager();
    virtual ~JointTrajectoryManager();

    /**
     * @brief set trajectory
     * @param trajectory new trajectory
     * @return void
     * @exception runtime_error a joint in trajectory is already part of an active goal
     * @details if there is an existing trajectory, it is preempted
     */
    void addToJointGoalMap(const std::vector<std::string>& jointNames, const actionlib_msgs::GoalID goalId);
    void removeFromJointGoalMap(const std::vector<std::string>& jointNames);
    void removeFromJointGoalMap(const actionlib_msgs::GoalID& goal);

protected:
    /**
     * @brief is goal active
     * @param goalId goal ID to check
     * @return bool active state
     * @details derived class must provide a way to check this
     */
    virtual bool isActive(const actionlib_msgs::GoalID& goalId) const = 0;
    std::string jointGoalMapToString(void) const;
    typedef std::map<std::string, actionlib_msgs::GoalID> JointGoalMap_type;
    JointGoalMap_type jointGoalMap;
};

#endif
