#ifndef FORCETRAJECTORY_H
#define FORCETRAJECTORY_H
#include "nasa_robodyn_controllers_core/TreeIkTrajectory.h"
#include "nasa_robodyn_controllers_core/Cartesian_HybCntrl.h"
#include "nasa_common_utilities/Logger.h"

class ForceTrajectory : public TreeIkTrajectory
{
public:
    ForceTrajectory();
    virtual ~ForceTrajectory() {}

    void setCartesianHybCntrl(boost::shared_ptr<Cartesian_HybCntrl> forceController_in)
    {
        forceController = forceController_in;
    }

    /**
     * @brief getPose get pose at a particular time
     * @param time time along trajectory (0 - getDuration())
     * @param stepPose pose corresponding to the time
     * @details each step in a joint trajectory contains the position, velocity, and acceleration
     */
    virtual void getPose(double time, KDL::JntArrayAcc& pose);

protected:
    boost::shared_ptr<Cartesian_HybCntrl> forceController;
    double lastTime;
    std::string logCategory;
};

#endif // FORCETRAJECTORY_H
