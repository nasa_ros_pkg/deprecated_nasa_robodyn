/**
 * @file GoalManager.h
 * @brief Defines the GoalManager class.
 * @author Ross Taylor
 * @date Aug 24, 2012
 */

#ifndef GOAL_MANAGER_H
#define GOAL_MANAGER_H

#include <actionlib_msgs/GoalStatus.h>

/**
 * @class GoalManager
 * @brief manages goal status.
 */
class GoalManager
{
public:
    GoalManager() : ready(true) {}
    GoalManager(const GoalManager& in) : goalStatus(in.getStatus()), ready(in.isReady()) {}
    virtual ~GoalManager() {}

    /**
     * @brief maintain a goalStatus.
     */
    inline const actionlib_msgs::GoalStatus& getStatus() const {return goalStatus;}
    inline void setGoalId(const actionlib_msgs::GoalID& goalId) {goalStatus.goal_id = goalId;}
    inline void setGoalId(const ros::Time& stamp, const std::string& id)
        {goalStatus.goal_id.stamp = stamp; goalStatus.goal_id.id = id;}
    inline void setStatus(const actionlib_msgs::GoalStatus::_status_type& status) {goalStatus.status = status;}
    inline void setText(const std::string& text) {goalStatus.text = text;}

    /**
     * @brief allow maintainance of a state outside of just the goal status message.
     */
    inline bool isReady() const {return ready;}
    inline void setReadyState(bool ready_in) {ready = ready_in;}
    void reset(bool readyState = true)
    {
        setReadyState(readyState);
        goalStatus = actionlib_msgs::GoalStatus();
    }

private:
    // message for status updates
    actionlib_msgs::GoalStatus goalStatus;
    // status bit
    bool ready;
};

#endif
