#ifndef TREE_ID_H
#define TREE_ID_H

#include "nasa_robodyn_controllers_core/KdlTreeId.h"
#include "nasa_robodyn_controllers_core/JointDynamicsData.h"
#include "nasa_robodyn_utilities/RateLimiter.h"

#include <nasa_r2_common_msgs/StringArray.h>
#include <sensor_msgs/JointState.h>
#include "nasa_r2_common_msgs/WrenchState.h"

class RosMsgTreeId
{
public:
    RosMsgTreeId();
    ~RosMsgTreeId();

    int setBaseFrame(const nasa_r2_common_msgs::StringArray& baseMsg, std::string& baseFrame);
    void setJointData(const sensor_msgs::JointState& actualState,
                      const sensor_msgs::JointState& desiredStates,                      
                      const nasa_r2_common_msgs::WrenchState& wrenchState, 
                      JointDynamicsData& jd);

    void getJointCommand(const JointDynamicsData& jd, sensor_msgs::JointState& tauFF);
    void getJointInertias(const JointDynamicsData& jd, sensor_msgs::JointState& Hv);
    void getSegmentForces(const JointDynamicsData& jd, nasa_r2_common_msgs::WrenchState& segForceMsg);
    void setEmptyTorqueMsg(const JointDynamicsData& jd, sensor_msgs::JointState& tauFF);
    bool GetCompletionMessage(RateLimiter& yl, nasa_r2_common_msgs::StringArray& completionMsg);

};

#endif
