#ifndef KDL_CHAIN_ID_INERTIA_H
#define KDL_CHAIN_ID_INERTIA_H

#include <kdl/chain.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/chainidsolver.hpp>
#include <kdl/rigidbodyinertia.hpp>

/**
 * \brief Recursive Newton Euler ID solver with composite rigid body inertia
 *
 * The algorithm implementation is based on the book "Rigid Body
 * Dynamics Algorithms" of Roy Featherstone, 2008
 * (ISBN:978-0-387-74314-1) See pages 96 and 107 for the pseudo-code.
 * 
 * It calculates the torques and composite inertias for the joints, 
 * given the motion of
 * the joints (q,qdot,qdotdot), external forces on the segments
 * (expressed in the segments reference frame) and the dynamical
 * parameters of the segments.  The composite inertias calculated 
 * are equivalent to the diagonal of the joint space inertia matrix, H.
 */
class KdlChainIdInertia {
    public:
        /**
         * Constructor for the solver, it will allocate all the necessary memory
         * \param chain The kinematic chain to calculate the inverse dynamics for, an internal copy will be made.
         * \param grav The gravity vector to use during the calculation.
         */
        KdlChainIdInertia(const KDL::Chain& chain,KDL::Vector grav);
        ~KdlChainIdInertia(){};
        
        /**
         * Function to calculate from Cartesian forces to joint torques.
         * Input parameters;
         * \param q The current joint positions
         * \param q_dot The current joint velocities
         * \param q_dotdot The current joint accelerations
         * \param f_ext The external forces (no gravity) on the segments
         * Output parameters:
         * \param torques the resulting torques for the joints
         * \param Hv  The composite rigid body inertia each joint sees
         */
        int CartToJnt(const KDL::JntArray &q, const KDL::JntArray &q_dot, const KDL::JntArray &q_dotdot, const KDL::Wrenches& f_ext,KDL::JntArray &torques, KDL::JntArray &Hv);

    private:
        KDL::Chain chain;
        unsigned int nj;
        unsigned int ns;
        std::vector<KDL::Frame> X;
        std::vector<KDL::Twist> S;
        std::vector<KDL::Twist> v;
        std::vector<KDL::Twist> a;
        std::vector<KDL::Wrench> f;
	std::vector<KDL::RigidBodyInertia> Ii;
        KDL::Wrench Fis;
        KDL::Twist ag;
};

#endif
