/**
 * @file MinJerkTrajectoryFactory.h
 * @brief Defines the MinJerkTrajectoryFactory class.
 * @author Ross Taylor
 * @date Oct 30, 2012
 */

#ifndef MIN_JERK_TRAJECTORY_FACTORY_H
#define MIN_JERK_TRAJECTORY_FACTORY_H

#include <nasa_robodyn_controllers_core/TrajectoryFactory.h>

class MinJerkJointTrajectoryFactory : public JointTrajectoryFactory<>
{
public:
    MinJerkJointTrajectoryFactory() : JointTrajectoryFactory<>() {}
    virtual ~MinJerkJointTrajectoryFactory() {}

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param startPose start joint positions
     * @param goalPose goal joint positions
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<JointTrajectory> getTrajectory(const KDL::JntArrayAcc& startPose,
                               const KDL::JntArrayAcc& goalPose, double durationTarget = -1.) const;
};

class MinJerkCartesianTrajectoryFactory : public CartesianTrajectoryFactory<>
{
public:
    MinJerkCartesianTrajectoryFactory() : CartesianTrajectoryFactory<>() {}
    virtual ~MinJerkCartesianTrajectoryFactory() {}

    /**
     * @brief getTrajectory
     * @param startPose start position
     * @param goalPose goal position
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<CartesianTrajectory> getTrajectory(const KDL::FrameAcc& startPose,
                               const KDL::FrameAcc& goalPose, double timeToFinish = -1.) const;
};

class MinJerkSynchedCartesianTrajectoryFactory : public SynchedCartesianTrajectoryFactory
{
public:
    MinJerkSynchedCartesianTrajectoryFactory() : SynchedCartesianTrajectoryFactory() {}
    virtual ~MinJerkSynchedCartesianTrajectoryFactory() {}

    /**
     * @brief getTrajectory
     * @param startPose start position
     * @param goalPose goal position
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<SynchedCartesianTrajectory> getTrajectory(const std::vector<KDL::FrameAcc>& startPoses,
                                        const std::vector<KDL::FrameAcc>& goalPoses, double timeToFinish = -1.) const;
};

#endif
