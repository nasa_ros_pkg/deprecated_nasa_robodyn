/**
 * @file RosMsgTreeIkTrajectoryFactory.h
 * @brief Defines the RosMsgTreeIkTrajectoryFactory classes.
 * @author Ross Taylor
 * @date Nov 1, 2012
 */

#ifndef ROS_MSG_TREE_IK_TRAJECTORY_FACTORY_H
#define ROS_MSG_TREE_IK_TRAJECTORY_FACTORY_H

#include <nasa_robodyn_controllers_core/TrajectoryFactory.h>
#include <nasa_robodyn_controllers_core/RosMsgTrajectory.h>
#include <nasa_robodyn_controllers_core/KdlTreeIk.h>
#include <nasa_robodyn_controllers_core/KdlTreeTr.h>
#include <nasa_robodyn_controllers_core/TreeIkTrajectory.h>

#include <nasa_r2_common_msgs/StringArray.h>

#include <stdexcept>
#include "nasa_common_utilities/Logger.h"
#include "nasa_robodyn_utilities/RosMsgConverter.h"

class RosMsgTreeIkTrajectoryFactory
{
public:
    RosMsgTreeIkTrajectoryFactory() {}
    virtual ~RosMsgTreeIkTrajectoryFactory() {}

    void setCartesianFactory(boost::shared_ptr<const SynchedCartesianTrajectoryFactory> factory_in)
    {
        factory = factory_in;
        sequenceFactory.reset(new sequenceFactoryType);
        sequenceFactory->setTrajectoryFactory(factory);
    }

    void setTreeIk(boost::shared_ptr<KdlTreeIk> treeIk_in)
    {
        treeIk = treeIk_in;
    }

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param jointPositions start joint positions
     * @param jointVels start joint velocities
     * @param prevJointVels previous joint velocities (joint states only hold pos & vel so the prev is used to derive acc)
     * @param poseState start cartesian poses
     * @param poseVels starting cartesian vels
     * @param goalTraj goal joint positions
     */
    virtual boost::shared_ptr<RosMsgJointTrajectory> getTrajectory(const sensor_msgs::JointState& jointPositions,
                                                                   const sensor_msgs::JointState& jointVels,
                                                                   const sensor_msgs::JointState& prevJointVels,
                                                                   const nasa_r2_common_msgs::PoseState& poseState,
                                                                   const nasa_r2_common_msgs::PoseState& poseVels,
                                                                   const nasa_r2_common_msgs::PoseTrajectory& goalTraj) const;

protected:
    boost::shared_ptr<KdlTreeIk> treeIk;

    typedef CartesianTrajectorySequenceFactory<SynchedTrajectory> sequenceFactoryType;
    boost::shared_ptr<sequenceFactoryType> sequenceFactory;
    boost::shared_ptr<const SynchedCartesianTrajectoryFactory> factory;
};

#endif
