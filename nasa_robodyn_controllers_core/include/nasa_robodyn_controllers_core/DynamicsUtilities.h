#ifndef DYNAMICS_UTILITIES_H
#define DYNAMICS_UTILITIES_H

#include <vector>

#include <kdl/jacobian.hpp>
#include <kdl/frames.hpp>
#include <kdl/jntarray.hpp>
#include <kdl/utilities/svd_eigen_HH.hpp>

class DynamicsUtilities
{
public:
    DynamicsUtilities();
    ~DynamicsUtilities();

    KDL::Wrench SumWrenches(const std::vector<KDL::Wrench>& f_center);

    bool isBaseFrame(const std::vector<std::string> baseFrames, const std::string frame);

    void MultiplyJacobianTranspose(const KDL::Jacobian& jac, const KDL::Wrench& src, KDL::JntArray& dest);
    
    int MultiplyJacobianTransposeInverse(const KDL::Jacobian& jac, const KDL::JntArray& src, KDL::Wrench& dest);

    double mag(const KDL::Vector& vec);

};

#endif
