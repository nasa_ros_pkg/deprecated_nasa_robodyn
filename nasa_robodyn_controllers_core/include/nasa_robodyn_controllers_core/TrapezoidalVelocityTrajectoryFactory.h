/**
 * @file TrapezoidalVelocityTrajectoryFactory.h
 * @brief Defines the TrapezoidalVelocityTrajectoryFactory class.
 * @author Ross Taylor
 * @date Jan 2, 2013
 */

#ifndef TRAP_VEL_TRAJECTORY_FACTORY_H
#define TRAP_VEL_TRAJECTORY_FACTORY_H

#include "nasa_robodyn_controllers_core/TrajectoryFactory.h"
#include "nasa_robodyn_controllers_core/TrapezoidalVelocityTrajectory.h"
#include "nasa_common_utilities/Logger.h"

class TrapezoidalVelocityJointTrajectoryFactory : public JointTrajectoryFactory<>
{
public:
    TrapezoidalVelocityJointTrajectoryFactory() : JointTrajectoryFactory<>() {}
    virtual ~TrapezoidalVelocityJointTrajectoryFactory() {}

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param startPose start joint positions
     * @param goalPose goal joint positions
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<JointTrajectory> getTrajectory(const KDL::JntArrayAcc& startPose,
                               const KDL::JntArrayAcc& goalPose, double durationTarget = -1.) const;
};

class TrapezoidalVelocityCartesianTrajectoryFactory : public CartesianTrajectoryFactory<>
{
public:
    TrapezoidalVelocityCartesianTrajectoryFactory() : CartesianTrajectoryFactory<>() {}
    virtual ~TrapezoidalVelocityCartesianTrajectoryFactory() {}

    /**
     * @brief getTrajectory
     * @param startPose start position
     * @param goalPose goal position
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<CartesianTrajectory> getTrajectory(const KDL::FrameAcc& startPose,
                               const KDL::FrameAcc& goalPose, double timeToFinish = -1.) const;
};

class TrapezoidalVelocitySynchedCartesianTrajectoryFactory : public SynchedCartesianTrajectoryFactory
{
public:
    TrapezoidalVelocitySynchedCartesianTrajectoryFactory() : SynchedCartesianTrajectoryFactory() {}
    virtual ~TrapezoidalVelocitySynchedCartesianTrajectoryFactory() {}

    /**
     * @brief getTrajectory
     * @param startPose start position
     * @param goalPose goal position
     * @param timeToFinish time to execute move
     * @details Derived classes should implement a trajectory generation that produces equally spaced (by timeStep)
     * points along the path without exceeding the velocity limits. If timeToFinish is larger than the time
     * required to satisfy velocity limits, the trajectory should take that long to finish.
     */
    virtual boost::shared_ptr<SynchedCartesianTrajectory> getTrajectory(const std::vector<KDL::FrameAcc>& startPoses,
                                        const std::vector<KDL::FrameAcc>& goalPoses, double timeToFinish = -1.) const;
};

#endif
