/**
 * @file TreeIkTrajectory.h
 * @brief Defines the TreeIkTrajectory class.
 * @author Ross Taylor
 * @date Nov 26, 2012
 */

#ifndef TREE_IK_TRAJECTORY_H
#define TREE_IK_TRAJECTORY_H

#include <nasa_robodyn_controllers_core/Trajectory.h>
#include <nasa_robodyn_controllers_core/KdlTreeIk.h>

class TreeIkTrajectory : public JointTrajectory
{
public:
    TreeIkTrajectory();
    virtual ~TreeIkTrajectory() {}

    void setCartesianTrajectory(boost::shared_ptr<SynchedCartesianTrajectory> traj_in)
    {
        setDuration(traj_in->getDuration());
        trajectory = traj_in;
    }

    void setTreeIk(boost::shared_ptr<KdlTreeIk> treeIk_in)
    {
        treeIk = treeIk_in;
    }

    void setInitialJoints(const KDL::JntArrayAcc& joints_in)
    {
        jointsLast = jointsInit = joints_in;
    }

    void setNodeNames(const std::vector<std::string>& nodeNames_in)
    {
        nodeNames = nodeNames_in;
    }

    void setNodePriorities(const std::vector<KdlTreeIk::NodePriority>& nodePriorities_in)
    {
        nodePriorities = nodePriorities_in;
    }

    /**
     * @brief getPose get pose at a particular time
     * @param time time along trajectory (0 - getDuration())
     * @param stepPose pose corresponding to the time
     * @details each step in a joint trajectory contains the position, velocity, and acceleration
     */
    virtual void getPose(double time, KDL::JntArrayAcc& pose);

protected:
    boost::shared_ptr<SynchedCartesianTrajectory> trajectory;
    boost::shared_ptr<KdlTreeIk> treeIk;
    KDL::JntArrayAcc jointsInit, jointsLast;
    double lastTime;
    std::vector<std::string> nodeNames;
    std::vector<KDL::FrameAcc> nodeFramesAcc;
    std::vector<KDL::Frame> nodeFrames;
    std::vector<KdlTreeIk::NodePriority> nodePriorities;

};

#endif
