#ifndef ROSMSGFORCETRAJECTORYFACTORY_H
#define ROSMSGFORCETRAJECTORYFACTORY_H

#include <stdexcept>

#include "nasa_robodyn_controllers_core/TrajectoryFactory.h"
#include "nasa_robodyn_controllers_core/RosMsgTrajectory.h"
#include "nasa_robodyn_controllers_core/KdlTreeIk.h"
#include "nasa_robodyn_controllers_core/ForceTrajectory.h"

#include "nasa_r2_common_msgs/ForceControlAxisArray.h"
#include "nasa_r2_common_msgs/StringArray.h"
#include "nasa_r2_common_msgs/ForceControlAxis.h"

#include "nasa_common_utilities/Logger.h"
#include "nasa_robodyn_utilities/RosMsgConverter.h"

class RosMsgForceTrajectoryFactory
{
public:
    RosMsgForceTrajectoryFactory();
    virtual ~RosMsgForceTrajectoryFactory() {}

    void setCartesianFactory(boost::shared_ptr<const SynchedCartesianTrajectoryFactory> factory_in)
    {
        factory = factory_in;
        sequenceFactory.reset(new sequenceFactoryType);
        sequenceFactory->setTrajectoryFactory(factory);
    }

    void setTreeIk(boost::shared_ptr<KdlTreeIk> treeIk_in)
    {
        treeIk = treeIk_in;
    }

    /********************************************************************************/
    void setForceController(boost::shared_ptr<Cartesian_HybCntrl> forceController_in)
    {
        forceController = forceController_in;
    }
    /********************************************************************************/

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param jointPositions start joint positions
     * @param jointVels start joint velocities
     * @param prevJointVels previous joint velocities (joint states only hold pos & vel so the prev is used to derive acc)
     * @param poseState start cartesian poses
     * @param poseVels starting cartesian vels
     * @param goalTraj goal joint positions
     */
    virtual boost::shared_ptr<RosMsgJointTrajectory> getTrajectory(const sensor_msgs::JointState& jointPositions,
                                                                   const sensor_msgs::JointState& jointVels,
                                                                   const sensor_msgs::JointState& prevJointVels,
                                                                   const nasa_r2_common_msgs::PoseState& poseState,
                                                                   const nasa_r2_common_msgs::PoseState& poseVels,
                                                                   const nasa_r2_common_msgs::PoseTrajectory& goalTraj,
                                                                   const nasa_r2_common_msgs::ForceControlAxisArray &forceControlAxes) const;

protected:
    boost::shared_ptr<KdlTreeIk> treeIk;
    /**************************************************/
    boost::shared_ptr<Cartesian_HybCntrl> forceController;
    /**************************************************/
    typedef CartesianTrajectorySequenceFactory<SynchedTrajectory> sequenceFactoryType;
    boost::shared_ptr<sequenceFactoryType> sequenceFactory;
    boost::shared_ptr<const SynchedCartesianTrajectoryFactory> factory;
private:
    const std::string logCategory;
};

#endif // ROSMSGFORCETRAJECTORYFACTORY_H
