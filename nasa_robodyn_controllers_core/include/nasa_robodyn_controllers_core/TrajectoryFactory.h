/**
 * @file TrajectoryFactory.h
 * @brief Defines the TrajectoryFactory classes.
 * @author Ross Taylor
 * @date Oct 30, 2012
 */

#ifndef TRAJECTORY_FACTORY_H
#define TRAJECTORY_FACTORY_H

#include <nasa_robodyn_controllers_core/MotionLimiter.h>
#include <kdl/frameacc.hpp>
#include <kdl/jntarrayacc.hpp>
#include <vector>
#include <boost/shared_ptr.hpp>
#include <nasa_robodyn_controllers_core/Trajectory.h>

template <class P, template <class> class trajType = Trajectory>
class TrajectoryFactory
{
public:
    TrajectoryFactory() {}
    virtual ~TrajectoryFactory() {}

    /**
     * @brief getTrajectory produces a trajectory object from the inputs
     * @param startPose start position
     * @param goalPose goal position
     * @param durationTarget time to execute move (certain factories may use other constraints to determine duration)
     */
    virtual boost::shared_ptr<trajType<P> > getTrajectory(const typename trajType<P>::poseType& startPose,
                               const typename trajType<P>::poseType& goalPose, double durationTarget = -1.) const = 0;
};

template <class P, template <class> class trajType = Trajectory>
class TrajectorySequenceFactory
{
public:
    typedef TrajectoryFactory<P, trajType> factoryType;

    TrajectorySequenceFactory() {}

    virtual ~TrajectorySequenceFactory() {}

    void setTrajectoryFactory(boost::shared_ptr<const TrajectoryFactory<P, trajType> > trajFactory_in)
    {
        trajFactory = trajFactory_in;
    }

    /**
     * @brief getTrajectory produces a trajectory sequence object from the inputs by successively calling trajFactory's getTrajectory
     * @param startPose start position
     * @param goalPoses goal positions
     * @param durationTargets times to execute moves (certain factories may use other constraints to determine durations)
     */
    virtual boost::shared_ptr<TrajectorySequence<P, trajType> > getTrajectory(const typename trajType<P>::poseType& startPose,
                                                                              const std::vector<typename trajType<P>::poseType>& goalPoses,
                                                                              std::vector<double> durationTargets = std::vector<double>()) const;

private:
    boost::shared_ptr<const TrajectoryFactory<P, trajType> > trajFactory;
};

template <template <class> class trajType = Trajectory>
class JointTrajectoryFactory : public TrajectoryFactory<KDL::JntArrayAcc, trajType>, public JointMotionLimiter
{
};

template <template <class> class trajType = Trajectory>
class JointTrajectorySequenceFactory : public TrajectorySequenceFactory<KDL::JntArrayAcc, trajType>
{
};

template <template <class> class trajType = Trajectory>
class CartesianTrajectoryFactory : public TrajectoryFactory<KDL::FrameAcc, trajType>, public CartesianMotionLimiter
{
};

template <template <class> class trajType = Trajectory>
class CartesianTrajectorySequenceFactory : public TrajectorySequenceFactory<KDL::FrameAcc, trajType>
{
};

typedef CartesianTrajectoryFactory<SynchedTrajectory> SynchedCartesianTrajectoryFactory;
typedef CartesianTrajectorySequenceFactory<SynchedTrajectory> SynchedCartesianTrajectorySequenceFactory;

template <class P, template <class> class trajType>
boost::shared_ptr<TrajectorySequence<P, trajType> > TrajectorySequenceFactory<P, trajType>::getTrajectory(const typename trajType<P>::poseType& startPose,
                                                                          const std::vector<typename trajType<P>::poseType>& goalPoses,
                                                                          std::vector<double> durationTargets) const
{
    boost::shared_ptr<TrajectorySequence<P, trajType> > trajSeq(new TrajectorySequence<P, trajType>);
    double durTarget;
    typename trajType<P>::poseType currStart = startPose;
    for (unsigned int i = 0; i < goalPoses.size(); ++i)
    {
        if (durationTargets.size() > i)
        {
            durTarget = durationTargets[i];
        }
        else
        {
            durTarget = -1.;
        }
        trajSeq->push_back(trajFactory->getTrajectory(currStart, goalPoses[i], durTarget));
        currStart = goalPoses[i];
    }

    return trajSeq;
}

#endif
