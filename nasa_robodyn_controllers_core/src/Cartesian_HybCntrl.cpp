#include "nasa_robodyn_controllers_core/Cartesian_HybCntrl.h"

Cartesian_HybCntrl::Cartesian_HybCntrl()
    :logCategory("gov.nasa.controllers.CartHybCntrl")
{
    linearRateLimiter.reset(new RateLimiter);
    angularRateLimiter.reset(new RateLimiter);
}

Cartesian_HybCntrl::~Cartesian_HybCntrl()
{
}

//! @brief defines the default gains for force control
void Cartesian_HybCntrl::setGain(double forceP, double torqueP, double forceI, double torqueI, double forceD, double torqueD, double forceWindup, double torqueWindup)
{
    fKp = forceP;
    tKp = torqueP;
    fKi = forceI;
    tKi = torqueI;
    fKd = forceD;
    tKd = torqueD;
    forceIntegralWindupLimit = forceWindup;
    torqueIntegralWindupLimit = torqueWindup;
    RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::DEBUG<<"set force and torque error gain";
}

//! @brief defines the maximum velocities and accelerations for force control
void Cartesian_HybCntrl::setPoseSettings(double linVel, double angVel, double linAcc, double angAcc)
{
    maxLinearVelocity = linVel;
    maxAngularVelocity = angVel;

    linearRateLimiter->setRateLimit(linAcc);
    angularRateLimiter->setRateLimit(angAcc);

    RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::DEBUG<<"set pose settings for maxLinear, maxAngular Velocity and RateLimiter";
}

void Cartesian_HybCntrl::setFilter(double filterAlpha)
{
    alpha = filterAlpha;
    RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::DEBUG<<"set filter alpha";
}

//! @brief replaces the current sensor-name map with a new map
void Cartesian_HybCntrl::setSensorNameMap(const std::map<std::string, std::string>& sensorNameMap)
{
    this->sensorNameMap = sensorNameMap;
}

void Cartesian_HybCntrl::updateSensorForces(const std::map<std::string, KDL::Wrench> &sensorForces)
{
    //! go through sensorForces and update map (additive, more than one topic supplies forces)
    for(sensorForceItr = sensorForces.begin(); sensorForceItr != sensorForces.end(); ++sensorForceItr)
    {
        //! if sensor force is not in map, initialize to zero
        if(sensorForceMap.find(sensorForceItr->first) == sensorForceMap.end())
        {
            sensorForceMap[sensorForceItr->first] = KDL::Wrench::Zero();
        }
        sensorForceMap[sensorForceItr->first] = alpha * sensorForceMap[sensorForceItr->first] + (1-alpha) * sensorForceItr->second;
    }
}

bool Cartesian_HybCntrl::setInitialPose(const std::string& frameName, const::KDL::Frame& pose)
{
    if(forceCntrlMap.find(frameName)==forceCntrlMap.end())
    {
        RCS::Logger::getCategory("gov.nasa.controllers.core.CartHybCntrl")<<log4cpp::Priority::ERROR<<frameName<<" not in force control";
        return false;
    }
    forceCntrlMap.at(frameName).pose = pose;
    return true;
}

/** @brief  creates the map between node name, axis index, and desired force magnitude
  */
void Cartesian_HybCntrl::createForceControlMap(std::map<std::string, std::pair<std::vector<int>, std::vector<double> > > forceNodes)
{
    forceCntrlMap.clear();
    for(std::map<std::string, std::pair<std::vector<int>, std::vector<double> > >::iterator itr = forceNodes.begin(); itr!= forceNodes.end(); ++itr)
    {
        if(itr->second.first.size() != itr->second.second.size())
        {
            RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::ERROR<<"force axes and magnitudes do not match for node "<<itr->first<<"!";
            continue;
        }

        ForceControl newForceControl;
        newForceControl.sensorName = getSensorNameFromNode(itr->first);

        for(unsigned int i = 0; i< itr->second.first.size(); ++i)
        {
            switch(itr->second.first.at(i))
            {
                case nasa_r2_common_msgs::ForceControlAxis::X:
                    newForceControl.x = true;
                    newForceControl.desiredForce.force.x(itr->second.second.at(i));
                    break;
                case nasa_r2_common_msgs::ForceControlAxis::Y:
                    newForceControl.y = true;
                    newForceControl.desiredForce.force.y(itr->second.second.at(i));
                    break;
                case nasa_r2_common_msgs::ForceControlAxis::Z:
                    newForceControl.z = true;
                    newForceControl.desiredForce.force.z(itr->second.second.at(i));
                    break;
                case nasa_r2_common_msgs::ForceControlAxis::ROLL:
                    newForceControl.roll = true;
                    newForceControl.desiredForce.torque.x(itr->second.second.at(i));
                    break;
                case nasa_r2_common_msgs::ForceControlAxis::PITCH:
                    newForceControl.pitch = true;
                    newForceControl.desiredForce.torque.y(itr->second.second.at(i));
                    break;
                case nasa_r2_common_msgs::ForceControlAxis::YAW:
                    newForceControl.yaw = true;
                    newForceControl.desiredForce.torque.z(itr->second.second.at(i));
                    break;
                default:
                    break;
            }
        }
        forceCntrlMap[itr->first] = newForceControl;
    }
}

void Cartesian_HybCntrl::updateFrames(std::vector<std::string> nodeNames, std::map<std::string, KDL::Frame> referenceFrameMap, std::vector<KDL::Frame> &nodeFrames, double dt)
{
    RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::DEBUG<<"updating frames..";

    if(nodeNames.size() != nodeFrames.size())
    {
        std::stringstream err;
        err << "nodeNames and nodeFrames must be the same size! nodeNames: "<<nodeNames.size()<<", nodeFrames: "<<nodeFrames.size();
        RCS::Logger::log(logCategory, log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
    }

    //! get latest sensor forces
    for(forceItr = forceCntrlMap.begin(); forceItr != forceCntrlMap.end(); ++forceItr)
    {
        sensorForceItr = sensorForceMap.find(forceItr->second.sensorName);
        if(sensorForceItr == sensorForceMap.end())
        {
            std::stringstream err;
            err << "Unable to find actual force for "<<forceItr->second.sensorName;
            RCS::Logger::getCategory(logCategory)<<log4cpp::Priority::ERROR<<err.str();
            throw std::runtime_error(err.str());
        }
        forceItr->second.sensorForce = sensorForceItr->second;
    }

    for(unsigned int i = 0; i<nodeNames.size(); ++i)
    {
        std::stringstream ss;
        //! If this is a force controlled node, modify the force controlled axes
        if((forceItr = forceCntrlMap.find(nodeNames.at(i))) != forceCntrlMap.end())
        {
            //! get the force frame from base
            try
            {
                forceFrame = referenceFrameMap.at(forceItr->first);
                RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "forceFrame " << nodeNames[i] << ": "
                                                      << forceFrame.p.x() << ", " << forceFrame.p.y() << ", " << forceFrame.p.z() << ", "
                                                      << forceFrame.M.GetRot().x() << ", " << forceFrame.M.GetRot().y() << ", " << forceFrame.M.GetRot().z();
            }
            catch(std::exception &e)
            {
                std::stringstream err;
                err<< "Could not find force frame "<< forceItr->first << " in referenceFrameMap!";
                RCS::Logger::log(logCategory, log4cpp::Priority::ERROR, err.str());
                throw std::runtime_error(err.str());
            }

            //! get the sensor frame from base
            try
            {
                sensorFrame = referenceFrameMap.at(forceItr->second.sensorName);
                RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "sensorFrame " << nodeNames[i] << ": "
                                                      << sensorFrame.p.x() << ", " << sensorFrame.p.y() << ", " << sensorFrame.p.z() << ", "
                                                      << sensorFrame.M.GetRot().x() << ", " << sensorFrame.M.GetRot().y() << ", " << sensorFrame.M.GetRot().z();
            }
            catch(std::exception &e)
            {
                std::stringstream err;
                err<< "Could not find sensor frame "<< forceItr->second.sensorName << " in referenceFrameMap!";
                RCS::Logger::log(logCategory, log4cpp::Priority::ERROR, err.str());
                throw std::runtime_error(err.str());
            }

            const KDL::Frame& currentFrame = forceItr->second.pose;

            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "current frame " <<forceItr->first <<": "
                                                  << currentFrame.p.x() << ", " << currentFrame.p.y() << ", " << currentFrame.p.z() << ", "
                                                  << currentFrame.M.GetRot().x() << ", " << currentFrame.M.GetRot().y() << ", " << currentFrame.M.GetRot().z();


            //! convert the current pose into the force frame
            KDL::Frame forcePose = forceFrame.Inverse()*forceItr->second.pose;
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "current pose in force frame" << nodeNames[i] <<": "
                                                  << forcePose.p.x() << ", " << forcePose.p.y() << ", " << forcePose.p.z() << ", "
                                                  << forcePose.M.GetRot().x() << ", " << forcePose.M.GetRot().y() << ", " << forcePose.M.GetRot().z();

            //! convert the desired frame into the force frame
            desiredPose = forceFrame.Inverse()*nodeFrames.at(i);
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "desired pose in force frame" << nodeNames[i] << ": "
                                                  << desiredPose.p.x() << ", " << desiredPose.p.y() << ", " << desiredPose.p.z() << ", "
                                                  << desiredPose.M.GetRot().x() << ", " << desiredPose.M.GetRot().y() << ", " << desiredPose.M.GetRot().z();

            //! convert the sensor frame into the force frame
            sensorPose =  sensorFrame.Inverse()*forceFrame;
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "sensor pose in force frame" << nodeNames[i] << ": "
                                                  << sensorPose.p.x() << ", " << sensorPose.p.y() << ", " << sensorPose.p.z() << ", "
                                                  << sensorPose.M.GetRot().x() << ", " << sensorPose.M.GetRot().y() << ", " << sensorPose.M.GetRot().z();

            //! find the force error in the force frame
            forceError = forceItr->second.desiredForce - sensorPose.Inverse(forceItr->second.sensorForce);
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "ForceError "<< nodeNames[i] << ": "
                                                  << forceError.force.x() <<", "<<forceError.force.y() << ", " <<forceError.force.z() << ", "
                                                  << forceError.torque.x()<< ", "<< forceError.torque.y() << ", "<< forceError.torque.z();

            //! find the integrator
            forceItr->second.integrator.force += fKi * forceError.force;
            forceItr->second.integrator.torque += tKi * forceError.torque;
            limitVector(-forceIntegralWindupLimit, forceIntegralWindupLimit, forceItr->second.integrator.force);
            limitVector(-torqueIntegralWindupLimit, torqueIntegralWindupLimit, forceItr->second.integrator.torque);
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "integrator " << nodeNames[i] << ": "
                                                  << forceItr->second.integrator.force.x() <<", "<<forceItr->second.integrator.force.y() << ", " <<forceItr->second.integrator.force.z() << ", "
                                                  << forceItr->second.integrator.torque.x()<< ", "<< forceItr->second.integrator.torque.y() << ", "<< forceItr->second.integrator.torque.z();

            //! find the derivative
            forceItr->second.derivative = (forceError - forceItr->second.error)/dt;
            forceItr->second.derivative.force = fKd * forceItr->second.derivative.force;
            forceItr->second.derivative.torque = tKd * forceItr->second.derivative.torque;
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "derivative "<< nodeNames[i] <<": "
                                                  << forceItr->second.derivative.force.x() <<", "<<forceItr->second.derivative.force.y() << ", " <<forceItr->second.derivative.force.z() << ", "
                                                  << forceItr->second.derivative.torque.x()<< ", "<< forceItr->second.derivative.torque.y() << ", "<< forceItr->second.derivative.torque.z();
            //! find the gain
            forceItr->second.error = forceError;
            forceItr->second.gain.force = fKp * forceItr->second.error.force;
            forceItr->second.gain.torque = tKp * forceItr->second.error.torque;
            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "gain " << nodeNames[i] << ": "
                                                  << forceItr->second.gain.force.x() <<", "<<forceItr->second.gain.force.y() << ", " <<forceItr->second.gain.force.z() << ", "
                                                  << forceItr->second.gain.torque.x()<< ", "<< forceItr->second.gain.torque.y() << ", "<< forceItr->second.gain.torque.z();

            //! find the required velocity
            forcePID = forceItr->second.gain + forceItr->second.derivative + forceItr->second.integrator;
            if(doFeedForward)
            {
                velocity = KDL::Twist(forcePID.force, forcePID.torque) + forceItr->second.prevVelocity;
            }
            else
            {
                velocity = KDL::Twist(forcePID.force, forcePID.torque);
            }
            limitVelocity(forceItr->second.prevVelocity, velocity);

            RCS::Logger::getCategory(logCategory) << log4cpp::Priority::DEBUG << "velocity "<< nodeNames[i] <<": "
                                                  << velocity.vel.x() <<", "<<velocity.vel.y() << ", " <<velocity.vel.z() << ", "
                                                  << velocity.rot.x()<< ", "<< velocity.rot.y() << ", "<< velocity.rot.z();

            //! replace the force controlled axes with the new velocity
            if(forceItr->second.x)
            {
                desiredPose.p.x(forcePose.p.x() + velocity[nasa_r2_common_msgs::ForceControlAxis::X]*dt);
            }
            if(forceItr->second.y)
            {
                desiredPose.p.y(forcePose.p.y() + velocity[nasa_r2_common_msgs::ForceControlAxis::Y]*dt);
            }
            if(forceItr->second.z)
            {
                desiredPose.p.z(forcePose.p.z() + velocity[nasa_r2_common_msgs::ForceControlAxis::Z]*dt);
            }
            if(forceItr->second.roll)
            {
                forcePose.M.DoRotX(velocity[nasa_r2_common_msgs::ForceControlAxis::ROLL]*dt);
                desiredPose.M.RotX(forcePose.M.GetRot().x());
            }
            if(forceItr->second.pitch)
            {
                forcePose.M.DoRotY(velocity[nasa_r2_common_msgs::ForceControlAxis::PITCH]*dt);
                desiredPose.M.RotY(forcePose.M.GetRot().y());
            }
            if(forceItr->second.yaw)
            {
                forcePose.M.DoRotZ(velocity[nasa_r2_common_msgs::ForceControlAxis::YAW]*dt);
                desiredPose.M.RotZ(forcePose.M.GetRot().z());
            }

            //! convert things back into the base frame
            nodeFrames.at(i) = forceFrame * desiredPose;
            forceItr->second.pose = nodeFrames.at(i);
            forceItr->second.prevVelocity = velocity;
        }
    }
}
