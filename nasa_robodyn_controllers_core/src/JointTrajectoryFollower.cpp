#include <nasa_robodyn_controllers_core/JointTrajectoryFollower.h>

#include "nasa_common_utilities/Logger.h"
/***************************************************************************//**
 *
 * @brief Constructor for Joint Trajectory Follower
 *
 ******************************************************************************/
JointTrajectoryFollower::JointTrajectoryFollower()
    : velocityFactor(1.)
{
    goalManager.setReadyState(false);
}
/***************************************************************************//**
 *
 * @brief Set Ready State to false and abort Joint Trajectory Follower
 *
 ******************************************************************************/
void JointTrajectoryFollower::abort(const std::string& msg)
{
    goalManager.setReadyState(false);
    goalManager.setStatus(actionlib_msgs::GoalStatus::ABORTED);
    goalManager.setText(msg);
}
/***************************************************************************//**
 *
 * @brief Set Ready State to false and preempt Joint Trajectory Follower
 *
 ******************************************************************************/
void JointTrajectoryFollower::preempt(const std::string& msg)
{
    goalManager.setReadyState(false);
    goalManager.setStatus(actionlib_msgs::GoalStatus::PREEMPTED);
    goalManager.setText(msg);
}
/***************************************************************************//**
 *
 * @brief
 *
 ******************************************************************************/
PreplannedJointTrajectoryFollower::PreplannedJointTrajectoryFollower()
{
    trajIt = trajectory.points.end();
}
/***************************************************************************//**
 *
 * @brief Set trajectory and velocity to new values and prepare preplanned joint for new trajectory
 * @param trajectoryIn New trajectory value for joint
 * @param velocityFactorIn New velocity factor value for joint
 * @throw runtime_error If Joint is already following a trajectory
 *
 ******************************************************************************/
void PreplannedJointTrajectoryFollower::setTrajectory(const trajectory_msgs::JointTrajectory& trajectoryIn, double velocityFactorIn)
{
    if (isActive())
    {
        //  active
        std::stringstream err;
        err << "PreplannedJointTrajectoryFollower::setTrajectory() - currently following a trajectory";
        RCS::Logger::log("gov.nasa.controllers.PreplannedJointTrajectoryFollower", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }

    velocityFactor = velocityFactorIn;
    trajectory = trajectoryIn;
    trajIt = trajectory.points.begin();
    goalManager.reset(true);
    goalManager.setGoalId(trajectory.header.stamp, trajectory.header.frame_id);
    if (trajIt == trajectory.points.end())
    {
        goalManager.setReadyState(false);
        goalManager.setStatus(actionlib_msgs::GoalStatus::REJECTED);
        goalManager.setText("No points in trajectory.");
    }
    else
    {
        goalManager.setStatus(actionlib_msgs::GoalStatus::PENDING);
        goalManager.setText("Joint trajectory received at follower.");
    }
}
/***************************************************************************//**
 *
 * @brief Get the next point in the trajectory for the preplanned joint
 * @param nextPoint Next joint state
 * @param timeFromStart Time from the start of beginning of trajectory
 * @throw runtime_error If trajectory is inactive, or if trajectory names, positions, velocities, accelerations are not properly sized
 *
 ******************************************************************************/
void PreplannedJointTrajectoryFollower::getNextPoint(sensor_msgs::JointState& nextPoint, ros::Duration& timeFromStart)
{
    if (!isActive())
    {
        // not active
        std::stringstream err;
        err << "PreplannedJointTrajectoryFollower::getNextPoint() - trajectory is inactive";
        RCS::Logger::log("gov.nasa.controllers.PreplannedJointTrajectoryFollower", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }

    nextPoint.header.frame_id = trajectory.header.frame_id;
    if (trajectory.joint_names.size() != trajIt->positions.size() || trajectory.joint_names.size() != trajIt->velocities.size()
            || trajectory.joint_names.size() != trajIt->accelerations.size())
    {
        std::stringstream err;
        err << "PreplannedJointTrajectoryFollower::getNextPoint() - trajectory names, positions, velocities, accelerations not properly sized";
        RCS::Logger::log("gov.nasa.controllers.PreplannedJointTrajectoryFollower", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
    nextPoint.name     = trajectory.joint_names;
    nextPoint.position = trajIt->positions;
    nextPoint.velocity.resize(trajIt->velocities.size());
    for (unsigned int i = 0; i < trajIt->velocities.size(); ++i)
    {
        nextPoint.velocity[i] = trajIt->velocities[i] * velocityFactor;
    }
    nextPoint.effort = trajIt->accelerations;
    timeFromStart = trajIt->time_from_start;
    if (++trajIt == trajectory.points.end())
    {
        goalManager.setReadyState(false);
        goalManager.setStatus(actionlib_msgs::GoalStatus::SUCCEEDED);
        goalManager.setText("Trajectory executed successfully!");
    }
    else
    {
        goalManager.setStatus(actionlib_msgs::GoalStatus::ACTIVE);
        goalManager.setText("Joint trajectory currently being executed.");
    }
}
/***************************************************************************//**
 *
 * @brief
 *
 ******************************************************************************/
OnlineJointTrajectoryFollower::OnlineJointTrajectoryFollower()
    : time(0.), timestep(0.)
{
}
/***************************************************************************//**
 *
 * @brief Set trajectory, velocity factor, and time step increments for an online joint
 * @param trajectoryIn
 * @param timestep_in
 * @param velocityFactorIn
 * @throw runtime_error If joint is currently following a trajectory
 *
 ******************************************************************************/
void OnlineJointTrajectoryFollower::setTrajectory(boost::shared_ptr<RosMsgJointTrajectory> trajectoryIn, double timestep_in, double velocityFactorIn)
{
    if (isActive())
    {
        //  active
        std::stringstream err;
        err << "OnlineJointTrajectoryFollower::setTrajectory() - currently following a trajectory";
        RCS::Logger::log("gov.nasa.controllers.OnlineJointTrajectoryFollower", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }

    velocityFactor = velocityFactorIn;
    trajectory     = trajectoryIn;
    goalManager.reset(true);
    goalManager.setGoalId(trajectory->getGoalId());
    time     = 0.;
    timestep = timestep_in;
    goalManager.setStatus(actionlib_msgs::GoalStatus::PENDING);
    goalManager.setText("Joint trajectory received at follower.");
}
/***************************************************************************//**
 *
 * @brief Get the next point in the trajectory for the online joint
 * @param nextPoint Next joint state
 * @param timeFromStart Time from the start of beginning of trajectory
 * @throw runtime_error If trajectory is inactive
 *
 ******************************************************************************/
void OnlineJointTrajectoryFollower::getNextPoint(sensor_msgs::JointState& nextPoint, ros::Duration& timeFromStart)
{
    if (!isActive())
    {
        // not active
        std::stringstream err;
        err << "OnlineJointTrajectoryFollower::getNextPoint() - trajectory is inactive";
        RCS::Logger::log("gov.nasa.controllers.OnlineJointTrajectoryFollower", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }

    bool deact = false;
    time += timestep;

    if (time >= trajectory->getDuration())
    {
        time = trajectory->getDuration();
        deact = true;
    }

    nextPoint.header.frame_id = trajectory->getGoalId().id;
    nextPoint.name            = trajectory->getJointNames();
    trajectory_msgs::JointTrajectoryPoint pose;
    trajectory->getPose(time, pose);
    nextPoint.position = pose.positions;
    nextPoint.velocity.resize(pose.velocities.size());
    for (unsigned int i = 0; i < pose.velocities.size(); ++i)
    {
        nextPoint.velocity[i] = pose.velocities[i] * velocityFactor;
    }
    nextPoint.effort = pose.accelerations;
    timeFromStart    = pose.time_from_start;
    if (deact)
    {
        goalManager.setReadyState(false);
        goalManager.setStatus(actionlib_msgs::GoalStatus::SUCCEEDED);
        goalManager.setText("Trajectory executed successfully!");
    }
    else
    {
        goalManager.setStatus(actionlib_msgs::GoalStatus::ACTIVE);
        goalManager.setText("Joint trajectory currently being executed.");
    }
}
