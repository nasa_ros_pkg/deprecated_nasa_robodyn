#include <nasa_robodyn_controllers_core/RosMsgTrajectory.h>
#include "nasa_common_utilities/Logger.h"
#include <tf_conversions/tf_kdl.h>

void RosMsgJointTrajectory::getPose(double time, trajectory_msgs::JointTrajectoryPoint& pose)
{
    static KDL::JntArrayAcc jntPose;
    trajectory->getPose(time, jntPose);
    pose.positions.resize(jntPose.q.rows());
    pose.velocities.resize(jntPose.qdot.rows());
    pose.accelerations.resize(jntPose.qdotdot.rows());
    for (unsigned int i = 0; i < jntPose.q.rows(); ++i)
    {
        pose.positions[i]     = jntPose.q(i);
        pose.velocities[i]    = jntPose.qdot(i);
        pose.accelerations[i] = jntPose.qdotdot(i);
    }
    pose.time_from_start = ros::Duration(time);
}

void RosMsgSynchedCartesianTrajectory::getPose(double time, nasa_r2_common_msgs::PoseTrajectoryPoint& pose)
{
    static SynchedCartesianTrajectory::poseType tmpCartPose;
    static geometry_msgs::Pose tmpPose;
    pose.positions.clear();
    pose.velocities.clear();
    pose.accelerations.clear();

    trajectory->getPose(time, tmpCartPose);
    for (SynchedCartesianTrajectory::poseType::const_iterator it = tmpCartPose.begin(); it != tmpCartPose.end(); ++it)
    {
        tf::PoseKDLToMsg(it->GetFrame(), tmpPose);
        pose.positions.push_back(tmpPose);
    }
    pose.time_from_start = ros::Duration(time);
}

