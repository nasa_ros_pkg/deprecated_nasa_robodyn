#include "nasa_robodyn_controllers_core/RosMsgTreeUtilities.h"

RosMsgTreeUtilities::RosMsgTreeUtilities()
{
    bases.clear();
    jointNames.clear();
}

//RosMsgTreeUtilities::~RosMsgTreeUtilities()
//{
//}

/** @brief loads a file which describes the kinematic tree and resets bases
  * @param fileName     the file that contains the description of the kinematic tree
  */
void RosMsgTreeUtilities::loadFromFile(const std::string &fileName)
{
    KdlTreeParser::loadFromFile(fileName);
    bases.clear();
    bases.push_back(getBaseName());
    jointNames.clear();
    getJointNames(jointNames);
}

/** @brief sets the base frame vector to valid base frames
  * @param baseMsg      the base message received from the system
  * @returns false      if unable to find base frame in current tree
  */
bool RosMsgTreeUtilities::setBaseFrame(const nasa_r2_common_msgs::StringArray &baseMsg)
{
    int n = baseMsg.data.size();

    //! check if bases have changed
    if (n > 0 && baseMsg.data != bases)
    {
        //! clear bases
        bases.clear();
        //! check to see if bases are valid
        for (unsigned int i = 0; i < baseMsg.data.size(); ++i)
        {
            if (tree.getSegment(baseMsg.data[i]) == tree.getSegments().end())
            {
                std::stringstream err;
                err << "Unable to set base! Couldn't find segment "<< baseMsg.data[i]<<" in tree";
                RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.setBaseFrame", log4cpp::Priority::ERROR, err.str());
                return false;
            }
            bases.push_back(baseMsg.data[i]);
        }
    }
    else
    {
        RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.setBaseFrame", log4cpp::Priority::INFO, "call to setBaseFrame but bases are 0 or haven't changed");
    }

    return true;
}

/** @brief sets the base frame vector to valid base frames
  * @param baseMsg      the base message received from the system
  * @returns false      if unable to find base frame in current tree
  */
bool RosMsgTreeUtilities::setBaseFrame(std::string &baseMsg)
{
    if (tree.getSegment(baseMsg) == tree.getSegments().end())
    {
        std::stringstream err;
        err << "Unable to set base! Couldn't find segment "<< baseMsg<<" in tree";
        RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.setBaseFrame", log4cpp::Priority::ERROR, err.str());
        return false;
    }

    bases.clear();
    bases.push_back(baseMsg);
    return true;
}

/** @brief updates a map of all the frames in poseState with their new location and orientations
  * @param poseState        the poseState calculated from forward kinematics
  * @param base             the base frame from which to calculate all the poses
  * @param frames           the output frame map
  */
void RosMsgTreeUtilities::getFrames(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::Frame> &frames)
{
    KDL::Frame  frame;
    std::string tipName;
    for(unsigned int i = 0; i < poseState.name.size(); ++i)
    {
        tipName = poseState.name[i];
        try
        {
            RosMsgConverter::PoseStateToFrame(poseState, base, tipName, frame );
            frames[tipName] = frame;
        }
        catch(std::exception)
        {
            RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.getFrames", log4cpp::Priority::WARN, "Squelched error.. continuing..");
        }
    }
}

void RosMsgTreeUtilities::getFrameVels(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::FrameVel> &framevels)
{
    RCS::Logger::getCategory("gov.nasa.controllers.RosMsgTreeUtilities")<<log4cpp::Priority::DEBUG<<"entered getFrameVels";
    KDL::FrameVel frameVel;
    std::string   tipName;
    if (framevels.size() != poseState.name.size())
    {
        RCS::Logger::getCategory("gov.nasa.controllers.RosMsgTreeUtilities")<<log4cpp::Priority::DEBUG<<"resizing getFrameVels";
        framevels.clear();
        for (unsigned int i = 0; i < poseState.name.size(); ++i)
        {
            framevels.insert(std::make_pair(poseState.name[i], KDL::FrameVel()));
        }
    }
    for(unsigned int i = 0; i < poseState.name.size(); ++i)
    {
        RCS::Logger::getCategory("gov.nasa.controllers.RosMsgTreeUtilities")<<log4cpp::Priority::DEBUG<<"entered getFrameVels";
        tipName = poseState.name[i];
        try
        {
            RosMsgConverter::PoseStateToFrameVel(poseState, base, tipName, frameVel );
            framevels[tipName] = frameVel;
        }
        catch(std::exception)
        {
            RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.getFrameVels", log4cpp::Priority::WARN, "Squelched error in getFrameVels.. continuing..");
        }
    }
}

void RosMsgTreeUtilities::getFrameAccs(const nasa_r2_common_msgs::PoseState& poseState, const std::string &base, std::map<std::string, KDL::FrameAcc> &frameaccs)
{
    KDL::FrameAcc frameAcc;
    std::string   tipName;
    for(unsigned int i = 0; i < poseState.name.size(); ++i)
    {
        tipName = poseState.name[i];
        try
        {
            RosMsgConverter::PoseStateToFrameAcc(poseState, base, tipName, frameAcc );
            frameaccs[tipName] = frameAcc;
        }
        catch(std::exception)
        {
            RCS::Logger::log("gov.nasa.controllers.RosMsgTreeUtilities.getFrameAccs", log4cpp::Priority::WARN, "Squelched error.. continuing..");
        }
    }
}
