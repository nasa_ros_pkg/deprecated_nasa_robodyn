#include <nasa_robodyn_controllers_core/JointTrajectoryManager.h>
#include "nasa_common_utilities/Logger.h"
/***************************************************************************//**
 *
 * @brief Constructor for the Joint Trajectory Manager (empty)
 *
 ******************************************************************************/
JointTrajectoryManager::JointTrajectoryManager()
{

}

JointTrajectoryManager::~JointTrajectoryManager()
{

}
/***************************************************************************//**
 *
 * @brief Add specified joints and goal ID pairs to the Joint Goal Map
 * @param jointNames Vector of joint names to add to the Goal Map
 * @param goalId Goal Ids to pair with the joint names and insert into the Joint Goal Map
 *
 ******************************************************************************/
void JointTrajectoryManager::addToJointGoalMap(const std::vector<std::string>& jointNames, const actionlib_msgs::GoalID goalId)
{
    // Note: This method assumes that no joints in trajectory are associated
    //     with any existing goalId's in the jointGoalMap.  This must be checked before this
    //     method is called!

    for (std::vector<std::string>::const_iterator nameIt = jointNames.begin();
            nameIt != jointNames.end(); ++nameIt)
    {
        jointGoalMap.insert(std::make_pair(*nameIt, goalId));
    }
}
/***************************************************************************//**
 *
 * @brief Remove the pairs with the specified jointNames from the Joint Goal Map
 * @param jointNames Pointer to vector of joint names to remove from the Joint Goal Map
 * @throw runtime_error If one of the joint names specified is an active goal
 *
 ******************************************************************************/
void JointTrajectoryManager::removeFromJointGoalMap(const std::vector<std::string>& jointNames)
{
    for (std::vector<std::string>::const_iterator nameIt = jointNames.begin(); nameIt != jointNames.end(); ++nameIt)
    {
        // check each name for conflict and remove
        std::map<std::string, actionlib_msgs::GoalID>::iterator goalIt = jointGoalMap.find(*nameIt);
        if (goalIt != jointGoalMap.end())
        {
            if (isActive(goalIt->second))
            {
                // currently active
                std::stringstream err;
                err << "JointTrajectoryManager::removeFromJointGoalMap() - trying to remove an active goal";
                RCS::Logger::log("gov.nasa.controllers.JointTrajectoryManager", log4cpp::Priority::ERROR, err.str());
                throw std::runtime_error(err.str());
                return;
            }
            else
            {
                jointGoalMap.erase(goalIt);
            }
        }
    }
}
/***************************************************************************//**
 *
 * @brief Remove the pair with the specified GoalID from the Joint Goal Map
 * @param goal Pointer to GoalID to remove from the Joint Goal Map
 * @throw runtime_error If one of the GoalIDs specified is an active goal
 *
 ******************************************************************************/
void JointTrajectoryManager::removeFromJointGoalMap(const actionlib_msgs::GoalID& goal)
{
    if (isActive(goal))
    {
        // currently active
        std::stringstream err;
        err << "JointTrajectoryManager::removeFromJointGoalMap() - trying to remove an active goal";
        RCS::Logger::log("gov.nasa.controllers.JointTrajectoryManager", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
    else
    {
        for (std::map<std::string, actionlib_msgs::GoalID>::iterator it = jointGoalMap.begin(); it != jointGoalMap.end();)
        {
            if (it->second.id == goal.id && it->second.stamp == goal.stamp)
            {
                jointGoalMap.erase(it++);
            }
            else
            {
                ++it;
            }
        }
    }
}

/***************************************************************************//**
 *
 * @brief Convert the Joint Goal Map into a String
 * @return The string stream representation of the Joint Goal Map
 *
 ******************************************************************************/
std::string JointTrajectoryManager::jointGoalMapToString(void) const
{
    std::stringstream msg;
    std::map<std::string, actionlib_msgs::GoalID>::const_iterator jointGoalIt;
    msg << std::endl << "jointGoalMap" << std::endl;
    msg << "------------" << std::endl;
    for (jointGoalIt=jointGoalMap.begin(); jointGoalIt!=jointGoalMap.end(); ++jointGoalIt)
    {
        msg << jointGoalIt->first << ": " << jointGoalIt->second.stamp << ", " << jointGoalIt->second.id << std::endl;
    }
    return msg.str();
}
