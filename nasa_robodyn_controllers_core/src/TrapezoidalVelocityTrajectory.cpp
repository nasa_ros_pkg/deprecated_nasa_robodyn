#include <nasa_robodyn_controllers_core/TrapezoidalVelocityTrajectory.h>

#include "nasa_common_utilities/Logger.h"
#include <tf/tf.h>
#include <tf_conversions/tf_kdl.h>
#include <limits>

TrapezoidalVelocityUtility::TrapezoidalVelocityUtility()
    : posInit(0.)
    , posFinal(0.)
    , velInit(0.)
    , velFinal(0.)
    , accelInit(0.)
    , accelFinal(0.)
    , t1(0.)
    , t2(0.)
    , t3(0.)
    , v1(0.)
    , a1(0.)
    , a2(0.)
{
}

TrapezoidalVelocityUtility::~TrapezoidalVelocityUtility()
{
}

void TrapezoidalVelocityUtility::setConstraints(double posInitIn, double posFinalIn, double velInitIn, double velFinalIn, double accelInitIn, double accelFinalIn)
{
    posInit    = posInitIn;
    posFinal   = posFinalIn;
    velInit    = velInitIn;
    velFinal   = velFinalIn;
    accelInit  = accelInitIn;
    accelFinal = accelFinalIn;
}

double TrapezoidalVelocityUtility::duration(double vmaxIn, double amaxIn)
{
    static const double eps = std::numeric_limits<float>::epsilon();

    if (fabs(posFinal - posInit) <= eps && fabs(velInit - velFinal) < eps)
    {
        v1 = velFinal;
        a1 = a2 = amaxIn;
        t1 = t2 = t3 = 0.;
        std::stringstream str;
        str << "duration t1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::DEBUG, str.str());
        return 0.;
    }
    else if (vmaxIn <= eps)
    {
        // movement needed but no movement allowed by velocity
        std::stringstream err;
        err << "duration() - move requested but velocity limited to zero";
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return 0.;
    }
    else if (amaxIn <= eps)
    {
        std::stringstream err;
        err << "duration() - max acceleration limited to zero";
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return 0.;
    }
    else if (fabs(velFinal) > vmaxIn)
    {
        // final velocity too high
        std::stringstream err;
        err << "duration() - final velocity larger than max";
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return 0.;
    }
    else
    {
        v1 = vmaxIn;
        a1 = a2 = amaxIn;
        calculateTimes(vmaxIn);
        std::stringstream str;
        str << "duration t1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::DEBUG, str.str());
        return t3;
    }
}

void TrapezoidalVelocityUtility::setDuration(double durationIn)
{
    static const double eps = std::numeric_limits<float>::epsilon();
    setDurationHelper(durationIn);

    if (t1 < 0)
    {
        a1 = -a1;
        setDurationHelper(durationIn);
    }

    if (t3 < t2)
    {
        a2 = -a2;
        setDurationHelper(durationIn);
    }

    if (t1 < -eps || t2 - t1 < -eps || t3 - t2 < -eps)
    {
        std::stringstream err;
        err << "setDuration() - unable to find a solution";
        err << "\n\ttimes: " << t1 << ", " << t2 << ", " << t3;
        err << "\n\tinputs: " << posInit << ", " << posFinal << ", " << velInit << ", " << velFinal;
        err << "\n\tconstraints: " << v1 << ", " << a1 << ", " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
    else
    {
        std::stringstream str;
        str << "setDuration t1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::DEBUG, str.str());
    }
}

void TrapezoidalVelocityUtility::setDurationHelper(double durationIn)
{
    static const double eps = std::numeric_limits<float>::epsilon();

    if (fabs(a1) < eps || fabs(a2) < eps)
    {
        std::stringstream err;
        err << "setDurationHelper() a1 or a2 == 0";
        err << "\n\tt1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
    double a = -0.5*(1/a1 + 1/a2);
    double b = velInit/a1 + velFinal/a2 + durationIn;
    double c = -0.5*(velFinal*velFinal/a2 + velInit*velInit/a1) + posInit - posFinal;
    double d = b*b - 4*a*c;

    if (d < eps)
    {
        d = 0;
    }

    if (fabs(b) <= eps)
    {
        t1 = t2 = 0.;
    }
    else if (fabs(a) <= eps)
    {
        double vel = -c/b;
        t1         = (vel - velInit)/a1;
        t2         = (velFinal - vel)/a2 + durationIn;
    }
    else
    {
        double vel1 = (-b - sqrt(d))/(2*a);
        double vel2 = (-b + sqrt(d))/(2*a);
        t1          = (vel2 - velInit)/a1;
        t2          = (vel1 - velInit)/a1;
    }
    t3 = durationIn;
    v1 = a1*t1 + velInit;
}

void TrapezoidalVelocityUtility::getTimes(double &t1Out, double &t2Out, double &t3Out) const
{
    t1Out = t1;
    t2Out = t2;
    t3Out = t3;
    std::stringstream str;
    str << "getTimes t1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
    RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::DEBUG, str.str());
}

void TrapezoidalVelocityUtility::calculateTimes(double vmaxIn)
{
    static const double eps = std::numeric_limits<float>::epsilon();

    bool success = false;
    for (unsigned int i = 0; i < 2; ++i)
    {
        for (unsigned int j = 0; j < 2; ++j)
        {
            for (unsigned int k = 0; k < 2; ++k)
            {
                calculateTimeHelper();
                if (t1 >= 0. && t2 >= 0. && t3 >= 0. && t3 >= t2 - eps && t2 >= t1 - eps)
                {
                    success = true;
                    break;
                }
                else
                {
                    double tempV1 = v1;
                    if (calculateMaxVel() && v1 <= vmaxIn)
                    {
                        if (std::signbit(tempV1)) 
                        {
                            v1 = -v1;
                        }
                        calculateTimeHelper();
                        if (t1 >= 0. && t2 >= 0. && t3 >= 0. && t3 >= t2 - eps && t2 >= t1 - eps)
                        {
                            success = true;
                            break;
                        }
                    }
                    v1 = tempV1;

                }
                a2 = -a2;
            }
            if (success) 
            {
                break;
            }
            a1 = -a1;
        }
        if (success) 
        {
            break;
        }
        v1 = -v1;
    }
    if (t1 < -eps || t2 - t1 < -eps || t3 - t2 < -eps)
    {
        std::stringstream err;
        err << "calculateTimes() - unable to find a solution";
        err << "\n\ttimes: " << t1 << ", " << t2 << ", " << t3;
        err << "\n\tinputs: " << posInit << ", " << posFinal << ", " << velInit << ", " << velFinal;
        err << "\n\tconstraints: " << v1 << ", " << a1 << ", " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
}

void TrapezoidalVelocityUtility::calculateTimeHelper()
{
    static const double eps = std::numeric_limits<float>::epsilon();
    if (fabs(a1) < eps || fabs(a2) < eps)
    {
        std::stringstream err;
        err << "calculateTimeHelper() a1 or a2 == 0";
        err << "\n\tt1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }
    t1 = (v1 - velInit)  / a1;
    t3 = (v1 - velFinal) / a2;
    double temp = (posFinal - ((-a2/2)*t3*t3 + v1*t3 +(a1/2)*t1*t1 + velInit*t1 + posInit));
    if (temp == 0.)
    {
        t2 = t1;
    }
    else if (v1 == 0.)
    {
        t2 = -1.;
    }
    else
    {
        t2 = temp / v1 + t1;
    }
    t3 += t2;
}

bool TrapezoidalVelocityUtility::calculateMaxVel()
{
    static const double eps = std::numeric_limits<float>::epsilon();
    if (fabs(a1) < eps || fabs(a2) < eps)
    {
        std::stringstream err;
        err << "calculateMaxVel() a1 or a2 == 0";
        err << "\n\tt1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return false;
    }
    double temp = (posFinal - posInit + velFinal*velFinal/(2*a2) + velInit*velInit/(2*a1))*a2;

    if (temp < 0.)
    {
        return false;
    }
    else
    {
        v1 = sqrt(temp);
        return true;
    }
}

double TrapezoidalVelocityUtility::p(double t) const
{
    if (t <= t1)
    {
        return (a1/2)*t*t + velInit*t + posInit;
    }
    else if (t <= t2)
    {
        return posInit + (a1/2)*t1*t1 + velInit*t1 + v1*(t-t1);
    }
    else
    {
        return posInit + (a1/2)*t1*t1 + velInit*t1 + v1*(t-t1) - (a2/2)*(t-t2)*(t-t2);
    }
}

double TrapezoidalVelocityUtility::v(double t) const
{
    if (t <= t1)
    {
        return a1*t + velInit;
    }
    else if (t <= t2)
    {
        return v1;
    }
    else
    {
        return v1 - a2*(t-t2);
    }
}

double TrapezoidalVelocityUtility::a(double t) const
{
    if (t <= t1)
    {
        return a1;
    }
    else if (t <= t2)
    {
        return 0.;
    }
    else
    {
        return -a2;
    }
}

void TrapezoidalVelocityUtility::setTimes(double t1In, double t2In, double t3In)
{
    static const double eps = std::numeric_limits<float>::epsilon();

    t1       = t1In;
    t2       = t2In - t1In;
    t3       = t3In - t2In;
    double A = posFinal - posInit - (velInit*t1 + velFinal*t3)/2.;
    double B = t3/2. + t2 + t1/2.;
    if (B > eps)
    {
        v1 = A/B;
    }
    if (t1 > eps)
    {
        a1 = (v1 - velInit)/t1;
    }
    if (t3 > eps)
    {
        a2 = (v1 - velFinal)/t3;
    }
    t2 += t1;
    t3 += t2;
    std::stringstream str;
    str << "setTimes t1 t2 t3 v1 a1 a2: " << t1 << " " << t2 << " " << t3 << " " << v1 << " " << a1 << " " << a2;
    RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityUtility", log4cpp::Priority::DEBUG, str.str());
}

TrapezoidalVelocityJointTrajectory::TrapezoidalVelocityJointTrajectory(const JointMotionLimiter& jml, const KDL::JntArrayAcc& startPose,
        const KDL::JntArrayAcc& goalPose, double durationIn)
    : JointTrajectory(), JointMotionLimiter(jml), constraintAxis(-1)
{
    if (startPose.q.rows() != goalPose.q.rows())
    {
        std::stringstream err;
        err << "TrapezoidalVelocityJointTrajectory::TrapezoidalVelocityJointTrajectory() - startPose and goalPose lengths don't match";
        RCS::Logger::log("gov.nasa.controllers.TrapezoidalVelocityJointTrajectory", log4cpp::Priority::ERROR, err.str());
        throw std::runtime_error(err.str());
        return;
    }

    utils.resize(startPose.q.rows());

    for (unsigned int i = 0; i < startPose.q.rows(); ++i)
    {
        utils[i].setConstraints(startPose.q(i), goalPose.q(i), startPose.qdot(i), goalPose.qdot(i), startPose.qdotdot(i), goalPose.qdotdot(i));

        double jointDur = utils[i].duration(getVelocityLimit(i), getAccelerationLimit(i));

        if (jointDur > durationIn)
        {
            durationIn = jointDur;
        }
    }

    setDuration(durationIn);
}

TrapezoidalVelocityJointTrajectory::~TrapezoidalVelocityJointTrajectory()
{

}

void TrapezoidalVelocityJointTrajectory::setDuration(double durationIn)
{
    JointTrajectory::setDuration(durationIn);

    double maxT1 = 0.;
    double maxT2 = 0.;
    double maxT3 = 0.;
    double t1, t2, t3;

    if (constraintAxis < 0)
    {
        for (unsigned int i = 0; i < utils.size(); ++i)
        {
            utils[i].getTimes(t1, t2, t3);
            if (t3 > maxT3)
            {
                constraintAxis = i;
                maxT1          = t1;
                maxT2          = t2;
                maxT3          = t3;
            }
        }
    }
    else
    {
        utils[constraintAxis].getTimes(maxT1, maxT2, maxT3);
    }

    if (durationIn > maxT3)
    {
        utils[constraintAxis].setDuration(durationIn);
        utils[constraintAxis].getTimes(maxT1, maxT2, maxT3);
    }

    for (unsigned int i = 0; i < utils.size(); ++i)
    {
        utils[i].setTimes(maxT1, maxT2, maxT3);
    }
}
void TrapezoidalVelocityJointTrajectory::getPose(double time, KDL::JntArrayAcc& pose)
{
    if (getDuration() == 0 && time > 0)
    {
        throw std::runtime_error("TrapezoidalVelocityJointTrajectory::getPose cannot get a pose for a zero duration trajectory with nonzero time");
        return;
    }

    pose.resize(utils.size());

    for (unsigned int i = 0; i < utils.size(); ++i)
    {
        pose.q(i)       = utils[i].p(time);
        pose.qdot(i)    = utils[i].v(time);
        pose.qdotdot(i) = utils[i].a(time);
    }
}

TrapezoidalVelocityCartesianTrajectory::TrapezoidalVelocityCartesianTrajectory(const CartesianMotionLimiter& cml,
        const KDL::FrameAcc& startPose, const KDL::FrameAcc& goalPose, double durationIn)
    : CartesianTrajectory(), CartesianMotionLimiter(cml), constraintAxis(-1)
{
    utils.resize(7);

    // linear
    double dur;
    for (unsigned int i = 0; i < 3; ++i)
    {
        utils[i].setConstraints(startPose.p.p(i), goalPose.p.p(i), startPose.p.v(i), goalPose.p.v(i), startPose.p.dv(i), startPose.p.dv(i));
        dur = utils[i].duration(getLinearVelocityLimit(), getLinearAccelerationLimit());
        if (dur > durationIn) durationIn = dur;
    }

    // angular
    double x, y, z, w;

    startPose.M.R.GetQuaternion(x, y, z, w);
    startQ.setValue(x, y, z, w);
    goalPose.M.R.GetQuaternion(x, y, z, w);
    goalQ.setValue(x, y, z, w);
    if (startQ.dot(goalQ) < 0.)
    {
        // reduce to the short angle case
        goalQ = tf::Quaternion(-goalQ.x(), -goalQ.y(), -goalQ.z(), -goalQ.w());
    }

    // get init and final rot vels
    tf::Quaternion startVel(startPose.M.w.x(), startPose.M.w.y(), startPose.M.w.z(), 0.);
    startVel = startVel*startQ*0.5;
    tf::Quaternion goalVel(goalPose.M.w.x(), goalPose.M.w.y(), goalPose.M.w.z(), 0.);
    goalVel = goalVel*goalQ*0.5;

    // quaternion x
    utils[3].setConstraints(startQ.x(), goalQ.x(), startVel.x(), goalVel.x(), 0., 0.);
    utils[4].setConstraints(startQ.y(), goalQ.y(), startVel.y(), goalVel.y(), 0., 0.);
    utils[5].setConstraints(startQ.z(), goalQ.z(), startVel.z(), goalVel.z(), 0., 0.);
    utils[6].setConstraints(startQ.w(), goalQ.w(), startVel.w(), goalVel.w(), 0., 0.);

    for (unsigned int i = 3; i < 7; ++i)
    {
        dur = utils[i].duration(getRotationalVelocityLimit(), getRotationalAccelerationLimit());
        if (dur > durationIn) durationIn = dur;
    }

    setDuration(durationIn);
}

TrapezoidalVelocityCartesianTrajectory::~TrapezoidalVelocityCartesianTrajectory()
{

}

void TrapezoidalVelocityCartesianTrajectory::setDuration(double durationIn)
{
    CartesianTrajectory::setDuration(durationIn);

    double maxT1 = 0.;
    double maxT2 = 0.;
    double maxT3 = 0.;
    double t1, t2, t3;

    if (constraintAxis < 0)
    {
        constraintAxis = 0;
        for (unsigned int i = 0; i < utils.size(); ++i)
        {
            utils[i].getTimes(t1, t2, t3);
            if (t3 > maxT3)
            {
                constraintAxis = i;
                maxT1          = t1;
                maxT2          = t2;
                maxT3          = t3;
            }
        }
    }
    else
    {
        utils[constraintAxis].getTimes(maxT1, maxT2, maxT3);
    }

    if (durationIn > maxT3)
    {
        utils[constraintAxis].setDuration(durationIn);
        utils[constraintAxis].getTimes(maxT1, maxT2, maxT3);
    }

    for (unsigned int i = 0; i < utils.size(); ++i)
    {
        utils[i].setTimes(maxT1, maxT2, maxT3);
    }
}
void TrapezoidalVelocityCartesianTrajectory::getPose(double time, KDL::FrameAcc& pose)
{
    pose.p.p.x(utils[0].p(time));
    pose.p.p.y(utils[1].p(time));
    pose.p.p.z(utils[2].p(time));
    tf::Quaternion stepQ(utils[3].p(time), utils[4].p(time), utils[5].p(time), utils[6].p(time));
    stepQ.normalize();
    pose.M = KDL::Rotation::Quaternion(stepQ.x(), stepQ.y(), stepQ.z(), stepQ.w());
}
