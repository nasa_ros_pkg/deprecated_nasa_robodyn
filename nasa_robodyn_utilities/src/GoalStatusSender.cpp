#include "nasa_robodyn_utilities/GoalStatusSender.h"
/***************************************************************************//**
 *
 * @brief Constructor for Goal Status Sender
 *
 ******************************************************************************/
GoalStatusSender::GoalStatusSender(const std::string &name)
{
    goalStatusMsg.header.frame_id = name;
}

GoalStatusSender::~GoalStatusSender()
{
}
/***************************************************************************//**
 *
 * @brief Send a status message
 * @param clear If "clear" is true, then clear the status message at the end
 *
 ******************************************************************************/
void GoalStatusSender::sendStatusMessage(bool clear)
{
    if (!goalStatusMsg.status_list.empty())
    {
        goalStatusMsg.header.stamp = ros::Time::now();
        writeStatus(goalStatusMsg);

        if (clear)
        {
            clearStatusMessage();
        }
    }
}
/***************************************************************************//**
 *
 * @brief Send a status message
 * @param status Status message to add
 * @param clear Sent after the status is added
 *
 ******************************************************************************/
void GoalStatusSender::sendStatusMessage(const actionlib_msgs::GoalStatus& status, bool clear)
{
    addStatus(status);
    sendStatusMessage(clear);
}
/***************************************************************************//**
 *
 * @brief Send a status message
 * @param clear If "clear" is true, then clear the status message at the end
 *
 ******************************************************************************/
void GoalStatusSender::sendStatusMessage(const ros::Time& stamp, const std::string& id,
        const actionlib_msgs::GoalStatus::_status_type& status,
        const std::string& text, bool clear)
{
    addStatus(stamp, id, status, text);
    sendStatusMessage(clear);
}
/***************************************************************************//**
 *
 * @brief Clear the status message
 *
 ******************************************************************************/
void GoalStatusSender::clearStatusMessage()
{
    goalStatusMsg.status_list.clear();
}
/***************************************************************************//**
 *
 * @brief Add status to the statuS_list
 * @param status New status to be added to the status_list
 * @return One less than the size of the status list
 *
 ******************************************************************************/
unsigned int GoalStatusSender::addStatus(const actionlib_msgs::GoalStatus& status)
{
    goalStatusMsg.status_list.push_back(status);
    return goalStatusMsg.status_list.size() - 1;
}
/***************************************************************************//**
 *
 * @brief Send a status message
 * @param clear If "clear" is true, then clear the status message at the end
 *
 ******************************************************************************/
unsigned int GoalStatusSender::addStatus(const ros::Time& stamp, const std::string& id,
        const actionlib_msgs::GoalStatus::_status_type& status,
        const std::string& text)
{
    actionlib_msgs::GoalStatus gStatus;
    gStatus.goal_id.stamp = stamp;
    gStatus.goal_id.id    = id;
    gStatus.status        = status;
    gStatus.text          = text;
    return addStatus(gStatus);
}
/***************************************************************************//**
 *
 * @brief Retrieve status at the given index
 * @param index The index to check in the status_list
 * @throw std::out_of_range If the index is larger than the size of the status_list
 * @return Return the status if the index is valid
 *
 ******************************************************************************/
actionlib_msgs::GoalStatus& GoalStatusSender::status(unsigned int index)
{
    if (index < goalStatusMsg.status_list.size())
    {
        return goalStatusMsg.status_list[index];
    }
    else
    {
        std::stringstream ss;
        ss << "Trying to access status " << index << " that is out of range " << goalStatusMsg.status_list.size();
        throw std::out_of_range(ss.str());
    }
}
/***************************************************************************//**
 *
 * @brief Retrieve status at the given index
 * @param index The index to check in the status_list
 * @throw std::out_of_range If the index is larger than the size of the status_list
 * @return Return the status if the index is valid
 *
 ******************************************************************************/
const actionlib_msgs::GoalStatus& GoalStatusSender::status(unsigned int index) const
{
    if (index < goalStatusMsg.status_list.size())
    {
        return goalStatusMsg.status_list[index];
    }
    else
    {
        std::stringstream ss;
        ss << "Trying to access status " << index << " that is out of range " << goalStatusMsg.status_list.size();
        throw std::out_of_range(ss.str());
    }
}
/***************************************************************************//**
 *
 * @brief Remove status at the given index
 * @param index The index to erase in the status_list
 * @throw std::out_of_range If the index is larger than the size of the status_list
 *
 ******************************************************************************/
void GoalStatusSender::removeStatus(unsigned int index)
{
    if (index < goalStatusMsg.status_list.size())
    {
        goalStatusMsg.status_list.erase(goalStatusMsg.status_list.begin() + index);
    }
    else
    {
        std::stringstream ss;
        ss << "Trying to remove status " << index << " that is out of range " << goalStatusMsg.status_list.size();
        throw std::out_of_range(ss.str());
    }
}

