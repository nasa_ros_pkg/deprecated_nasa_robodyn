#include "nasa_robodyn_utilities/RateLimiter.h"

RateLimiter::RateLimiter():
    rateLimit(1.0),
    complete(0)
{
}

RateLimiter::~RateLimiter()
{
}

void RateLimiter::setRateLimit(double lim)
{
    rateLimit = lim;
}

double RateLimiter::getLimitedValue(double current, double previous)
{
    double diff   = current - previous;
    double output = current;

    if (diff < -rateLimit)
    {
        output = previous - rateLimit;
    }

    if (diff > rateLimit)
    {
        output = previous + rateLimit;
    }

    setCompletionCondition(diff, rateLimit);

    return output;
}

int RateLimiter::getLimitedValue(int current, int previous)
{
    int diff = current - previous;

    int lim = (int)rateLimit;
    if (lim < 1)
    {
        lim = 1;
    }

    int output = current;

    if (diff < -lim)
    {
        output = previous - lim;
    }

    if (diff > lim)
    {
        output = previous + lim;
    }

    setCompletionCondition(diff, lim);

    return output;
}

int RateLimiter::getCompletionCondition()
{
    return complete;
}

void RateLimiter::setCompletionCondition(double diff, double lim)
{
    if (std::abs(diff) > lim)
    {
        complete = 0; // false
    }
    else // diff < lim
    {
        complete = 1;
    }
}

void RateLimiter::setCompletionCondition(int diff, int lim)
{
    if (std::abs(diff) > lim)
    {
        complete = 0; // false
    }
    else // diff < lim
    {
        complete = 1; // true
    }
}
