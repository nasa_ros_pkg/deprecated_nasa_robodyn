#ifndef RATE_LIMITER_H
#define RATE_LIMITER_H

#include <cmath>

class RateLimiter {

public:

    RateLimiter();
    ~RateLimiter();

    void setRateLimit(double lim);
    double getLimitedValue(double current, double previous);
    int getLimitedValue(int current, int previous);
    int getCompletionCondition();
                       
private:
    void setCompletionCondition(double diff, double lim);
    void setCompletionCondition(int diff, int lim);
    double rateLimit;
    int complete;  // -1 idle, 0 false, 1 complete

};

#endif
