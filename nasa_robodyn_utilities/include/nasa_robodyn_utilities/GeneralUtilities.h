/**
 * @file GeneralUtilities.h
 * @brief Defines simple utilities that may be useful multiple places
 * @author Ross Taylor
 * @date 2013-09-09
 */
#ifndef GENERALUTILITIES_H
#define GENERALUTILITIES_H

//! namespace because of simple names
namespace Robodyn
{
/**
 * @brief limit limitVal to within min & max; if maxVal < minVal, behavior is undefined
 */
template <class T>
void limit(const T& minVal, const T& maxVal, T& limitVal)
{
    if (limitVal > maxVal) limitVal = maxVal;
    else if (limitVal < minVal) limitVal = minVal;
}
}

#endif
