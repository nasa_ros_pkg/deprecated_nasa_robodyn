/**
 * @file GoalStatusSender.h
 * @brief Defines the GoalStatusSender class.
 * @author Ross Taylor
 */
#ifndef GOALSTATUSSENDER_H
#define GOALSTATUSSENDER_H

#include <actionlib_msgs/GoalStatusArray.h>

/**
 * @brief The GoalStatusSender class handles goal status messages. A pure virtual writeStatus function
 * is used to actually send the message.
 */
class GoalStatusSender
{
public:
    GoalStatusSender(const std::string& name);

    virtual ~GoalStatusSender();

    void sendStatusMessage(bool clear = false);
    void sendStatusMessage(const actionlib_msgs::GoalStatus& status, bool clear = false);
    void sendStatusMessage(const ros::Time& stamp, const std::string& id,
                   const actionlib_msgs::GoalStatus::_status_type& status,
                   const std::string& text, bool clear = false);
    void clearStatusMessage();
    /**
     * @brief addStatus
     * @return index of status in array
     */
    unsigned int addStatus(const actionlib_msgs::GoalStatus& status);
    /**
     * @brief addStatus
     * @return index of status in array
     */
    unsigned int addStatus(const ros::Time& stamp, const std::string& id,
                   const actionlib_msgs::GoalStatus::_status_type& status,
                   const std::string& text);

    actionlib_msgs::GoalStatus& status(unsigned int index);
    const actionlib_msgs::GoalStatus& status(unsigned int index) const;

    /**
     * @brief removeStatus
     * @param index
     * @exception out_of_range index out of range
     */
    void removeStatus(unsigned int index);

protected:
    // virtual function that does the actual sending
    virtual void writeStatus(const actionlib_msgs::GoalStatusArray& goalStatusMsg_out) = 0;

private:
    actionlib_msgs::GoalStatusArray goalStatusMsg;
};

#endif
